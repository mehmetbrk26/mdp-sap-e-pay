sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/ui/core/Fragment",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV"
], function (Controller, JSONModel, MessageToast, Fragment, MessageBox, Filter, FilterOperator, Export, ExportTypeCSV) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler", {

		onInit: function (e) {
			var that = this;
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					// that.getView().byId("OnayBekTalepTable").setBusy(true);
					that.counter(that);
				}
			});
		},

		getData: function (e) {
			var that = this;
			var OnayBekTalepModel = new JSONModel();
			OnayBekTalepModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OnayBekTalepSet?$filter=Pernr eq '" + sap.ui.controller(
				"MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
			OnayBekTalepModel.attachRequestCompleted(function onComplete(oEvent) {
				sap.ui.core.BusyIndicator.show();
				if (oEvent.getParameter("success")) {
					// that.getView().byId("OnayBekTalepTable").setBusy(false);
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					that.getView().setModel(newModel, "OnayBekTalep");
					that.handleIconTabBarSelect();
					that.showTotalCost();
					// that.getView().byId("OnayBekTalepTable").setBusy(false);
				}
				// sap.ui.core.BusyIndicator.hide();
			});
		},

		counter: function (e) {
			// this.getView().byId("OnayBekTalepTable").setBusy(true);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var routeParam = oRouter.getRoute("Target_Dashboard")._aRoutes[0]._router._prevRoutes[0].params[0];
			this.getView().byId("IconTabBar").setSelectedKey(routeParam);
			this.getData(e);
			this.handleIconTabBarSelect(e);
		},

		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		handleIconTabBarSelect: function (e) {
			// sap.ui.core.BusyIndicator.show();
			var that = this;
			var tab = this.getView().byId("IconTabBar").getSelectedKey();
			var table = this.getView().byId("OnayBekTalepTable");
			// table.setBusy(true);
			var aFilter = new sap.ui.model.Filter({
				and: true,
				filters: []
			});
			if (tab === "1") {
				aFilter.aFilters.push(
					new sap.ui.model.Filter("Onyst", sap.ui.model.FilterOperator.EQ, "01")
				);
				aFilter.aFilters.push(
					new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "02")
				);
				that.getView().byId("btnAccept").setEnabled(true);
				that.getView().byId("btnReject").setEnabled(true);
				that.getView().byId("OnayBpbak").setVisible(true);
				that.getView().byId("OnayVdbak").setVisible(true);
				that.getView().byId("OnayUpbak").setVisible(true);
				that.getView().byId("Mupbak").setVisible(true);
				that.getView().byId("redndn").setVisible(true);
			} else if (tab === "2") {
				aFilter.aFilters.push(
					new sap.ui.model.Filter("Onyst", sap.ui.model.FilterOperator.EQ, "02")
				);
				that.getView().byId("btnAccept").setEnabled(false);
				that.getView().byId("btnReject").setEnabled(false);
				that.getView().byId("OnayBpbak").setVisible(false);
				that.getView().byId("OnayVdbak").setVisible(false);
				that.getView().byId("OnayUpbak").setVisible(false);
				that.getView().byId("Mupbak").setVisible(false);
				that.getView().byId("redndn").setVisible(false);
			} else if (tab === "3") {
				aFilter.aFilters.push(
					new sap.ui.model.Filter("Onyst", sap.ui.model.FilterOperator.EQ, "03")
				);
				that.getView().byId("btnAccept").setEnabled(false);
				that.getView().byId("btnReject").setEnabled(false);
				that.getView().byId("OnayBpbak").setVisible(false);
				that.getView().byId("OnayVdbak").setVisible(false);
				that.getView().byId("OnayUpbak").setVisible(false);
				that.getView().byId("Mupbak").setVisible(false);
				that.getView().byId("redndn").setVisible(false);
			} else {
				that.getView().byId("OnayBpbak").setVisible(true);
				that.getView().byId("OnayVdbak").setVisible(true);
				that.getView().byId("OnayUpbak").setVisible(true);
				that.getView().byId("Mupbak").setVisible(true);
			}
			aFilter.aFilters.push(
				new sap.ui.model.Filter("Pernr", sap.ui.model.FilterOperator.EQ, sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr())
			);

			that.showTotalCost();
			table.getBinding("rows").filter([aFilter]);
			// table.setBusy(false);
			// table.setBusy(false);
			table.setVisibleRowCountMode("Fixed");
			var x = window.innerHeight - 213;
			var count = Math.floor(x / 45);
			table.setVisibleRowCount(count - 3);

		},
		showTotalCost: function () {
			// sap.ui.core.BusyIndicator.show();
			var that = this;
			var table = this.getView().byId("OnayBekTalepTable");
			var tab = this.getView().byId("IconTabBar").getSelectedKey();
			var model = this.getView().getModel("OnayBekTalep");
			if (model) {
				var data = model.getProperty("/dataList");
				if (data.length > 0) {
					var filterData = data.filter(function (fil) {
						// return fil.Onyst === "0" + tab && fil.Ostat === "02" && tab !== 1 ? fil;
						// return fil.Onyst === "0" + tab && tab !== 1 ? fil : fil.Ostat === "02" && fil;
						// return fil.Onyst === "0" + tab && tab !== "1" ? fil : fil.Ostat === "02" && fil;
						if (tab === "1") {
							return fil.Onyst === "0" + tab && fil.Ostat === "02";
						} else if (tab === "2" || tab === "3") {
							return fil.Onyst === "0" + tab;
						} else {
							return fil;
						}
					});

					table.setVisibleRowCountMode("Fixed");

					var x = window.innerHeight - 213;
					var count = Math.floor(x / 45);
					table.setVisibleRowCount(count - 3);

					table.setAlternateRowColors(true);
					var costs = [];
					for (var j = 0; j < filterData.length; j++) {
						costs.push({
							pb: filterData[j].Waers,
							cost: parseFloat(filterData[j].Odtut)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});
					// console.log(result);
				}
			}
			// that.getView().byId("OnayBekTalepTable").setBusy(true);
			var costModel = new JSONModel(result);
			costModel.setProperty("/dataList", result);
			this.getView().setModel(costModel, "costData");
			// table.setBusy(false);
			// that.getView().byId("OnayBekTalepTable").setBusy(false);
			// sap.ui.core.BusyIndicator.hide();
		},

		OnayBekTalepApprove: function (e) {
			var that = this;
			var table = this.getView().byId("OnayBekTalepTable");
			var selectedIndices = table.getSelectedIndices();
			var temp = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				temp.push(table.getContextByIndex(selectedIndices[i]).getObject().Odmfn);
			}
			var data = {
				"Prcss": "Onay",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
				"YonOnayTalep": []
			};

			for (var x = 0; x < temp.length; x++) {
				var Selectedrow = {
					"Odmfn": temp[x]
				};
				data.YonOnayTalep.push(Selectedrow);
			}

			that.createModel = new JSONModel(data);
			that.getView().setModel(that.createModel, "createCollection");
			var oSOData = this.getView().getModel("createCollection").getData();

			var oModel = this.getOwnerComponent().getModel();

			oModel.create("/ProcessSet", oSOData, {
				method: "POST",
				header: "Content-Type: application/json",
				success: function (scc) {
					// var view = that.getView();

					// var oldmodel = that.getView().getModel("OnayBekTalep");
					// var olddata = oldmodel.getProperty("/dataList");
					// for (var i = 0; seilenkayit.length; i++) {
					// 	olddata[seilen kayitindex].Onyst = "02";
					// 	olddata[seilen kayitindex].Messega = "success";
					// }
					// oldmodel.setProperty("/dataList", olddata);
					// that.getView().setModel(oldmodel, "OnayBekTalep");

					MessageToast.show("Onaylama İşleminiz Başarı ile Gerçekleştirildi.");
					// table.setBusy(true);
					that.getView().byId("OnayBekTalepTable").getBinding("rows").refresh();
					that.getView().byId("OnayBekTalepTable").clearSelection();
					that.getData();
					// table.setBusy(false);
					// oModel.refresh();
				},
				error: function (err) {
					MessageToast.show("Onaylama İşlemi Sırasında Bir Hata Oluştu.");
				}
			});
		},

		OnayBekTalepReject: function (e) {
			var that = this;
			var table = this.getView().byId("OnayBekTalepTable");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("OnayBekTalep");
			var temp = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				temp.push(table.getContextByIndex(selectedIndices[i]).getObject().Odmfn);
			}
			var data = {
				"Prcss": "Red",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
				"YonOnayTalep": []
			};

			for (var x = 0; x < temp.length; x++) {
				var Selectedrow = {
					"Odmfn": temp[x],
					// "Redndn": model.getData().dataList[selectedIndices[x]].Redndn
					// "Redndn": table.getContextByIndex(selectedIndices[x]).getObject().Redndn
					"Redndn": table.getRows()[selectedIndices[x]].getCells()[14].getValue()
				};
				data.YonOnayTalep.push(Selectedrow);
			}
			var control = [];
			for (var y = 0; y < data.YonOnayTalep.length; y++) {
				control.push(data.YonOnayTalep[y].Redndn);
			}
			if (control.includes("")) {
				MessageBox.warning("Lütfen Reddetme Nedenlerini Dolu giriniz.");
			} else {

				that.createModel = new JSONModel(data);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();

				var oModel = this.getOwnerComponent().getModel();
				oModel.create("/ProcessSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						MessageToast.show("Reddetme İşleminiz Başarı ile Gerçekleşti.");
						// table.setBusy(true);
						that.getView().byId("OnayBekTalepTable").getBinding("rows").refresh();
						that.getView().byId("OnayBekTalepTable").clearSelection();
						that.getData();
						// table.setBusy(false);
						// oModel.refresh();
					},
					error: function (err) {
						MessageToast.show("Reddetme İşlemi Sırasında Bir Hata Oluştu.");
					}
				});
			}
		},

		OnayBekTalepSendChange: function (e) {
			var that = this;
			var table = this.getView().byId("OnayBekTalepTable");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("OnayBekTalep");
			var temp = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				temp.push(table.getContextByIndex(selectedIndices[i]).getObject().Odmfn);
				// reject.push(this.getView().byId("rejectText").getValue());
			}
			var data = {
				"Prcss": "Duzeltme",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
				"YonOnayTalep": []
			};

			for (var x = 0; x < temp.length; x++) {
				var Selectedrow = {
					"Odmfn": temp[x],
					"Duzndn": model.getData().dataList[selectedIndices[x]].Duzndn
				};
				data.YonOnayTalep.push(Selectedrow);
			}
			var control = [];
			for (var y = 0; y < data.YonOnayTalep.length; y++) {
				control.push(data.YonOnayTalep[y].Duzndn);
			}
			if (control.includes("")) {
				MessageBox.warning("Düzeltemeye Gönder Nedenlerini Dolu giriniz.");
			} else {
				that.createModel = new JSONModel(data);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();

				var oModel = this.getOwnerComponent().getModel();
				// var mParameters = {
				// 	success: that._handleCreateSuccess,
				// 	error: that._handleCreateError
				// };
				// oModel.create("/ProcessSet", oSOData, mParameters);
				oModel.create("/ProcessSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						// var view = that.getView();
						// that.deleteData(e, view, "OnayBekTalepTable", "OnayBekTalep", "/dataList");
						MessageToast.show("Formnlarınız Başarı ile Düzenlemeye Gönderildi.");
						oModel.refresh();
					},
					error: function (err) {
						MessageToast.show("Düzenleme Gönder İşlemi Sırasında Bir Hata Oluştu.");
					}
				});
			}
		},
		showDetailOdemeFormn: function (e) {
			var view = this.getView();
			var table = this.getView().byId("OnayBekTalepTable");
			this.ShowDetailOdemeFormn(e, table, view);
		},
		ShowDetailOdemeFormn: function (oEvent, table, view) {
			var that = this;
			var Cari = table.getContextByIndex(oEvent.getSource().getParent().getIndex()).getObject().Cari;
			var model = new JSONModel();
			var Odmfn = oEvent.getSource().getText();

			model.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OdmfnDetaySet?$filter=Odmfn eq '" + Odmfn + "'");
			model.attachRequestCompleted(function onComplete(e) {
				if (e.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var costs = [];
					var data = e.getSource().getData().d.results;
					var detailModel = new JSONModel();
					detailModel.setProperty("/dataList", data);
					if (data.length === 0) {
						MessageBox.warning(Odmfn + " " + "numaralı formun detay bilgisi bulunmamaktadır!");
					} else {
						if (!that.oDialog2) {
							that.oDialog2 = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.YoneticiOdemeFormNoDetay", this);
							view.addDependent(that.oDialog2);
						}
						that.oDialog2.removeAllButtons();
						that.oDialog2.addButton(new sap.m.Button({
							text: "Kapat",
							press: function () {
								that.oDialog2.close();
								that.oDialog2.destroy();
								that.oDialog2 = null;
							}
						}));
						that.oDialog2.open();

						if (Cari === "") {
							var detailTable = sap.ui.getCore().byId("detailTable");
							var detailObj = {
								view: view,
								table: detailTable,
								Odmfn: Odmfn
							};
							// sap.ui.getCore().byId("detailLifnr").getTemplate().attachPress(that.detailLifnr.bind(view, detailObj));
							// detailTable.setRowActionCount(1);
							// detailTable.setRowActionTemplate(new sap.ui.table.RowAction({
							// 	items: [
							// 		new sap.ui.table.RowActionItem({
							// 			icon: "sap-icon://display",
							// 			text: "Detayları Görüntüle",
							// 			press: that.detailLifnr.bind(view, detailObj)
							// 		})
							// 	]
							// }));
							var columnsList = detailTable.getColumns().reverse();
							var column = new sap.ui.table.Column({
								label: "Satıcı Numarası",
								hAlign: sap.ui.core.TextAlign.Start,
								autoResizable: true,
								width: "100px",
								template: new sap.m.Button({
									text: "{formnDetailData>Lifnr}",
									press: that.detailLifnr.bind(view, detailObj)
								})
							});
							columnsList.push(column);
							columnsList = columnsList.reverse();
							detailTable.addColumn(columnsList[1]);
							for (var j = 0; j < columnsList.length; j++) {
								if (j !== 1) {
									detailTable.addColumn(columnsList[j]);
								}
							}
							for (var i = 0; i < data.length; i++) {
								detailModel.getData().dataList[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
									detailModel.getData().dataList[i].Vadet);
								costs.push({
									pb: detailModel.getData().dataList[i].Waers,
									cost: parseFloat(detailModel.getData().dataList[i].Odmtu)
								});
							}
							view.setModel(detailModel, "formnDetailData");
							that.findTotalCost(detailModel, costs, view);
							sap.ui.getCore().byId("detailBukrs").setVisible(false);
							sap.ui.getCore().byId("detailBelnr").setVisible(false);
							sap.ui.getCore().byId("detailGjahr").setVisible(false);
							sap.ui.getCore().byId("detailWrbtr").setVisible(false);
							sap.ui.getCore().byId("detailSgtxt").setVisible(false);
							sap.ui.getCore().byId("detailXblnr").setVisible(false);
						} else {
							for (var j = 0; j < data.length; j++) {
								detailModel.getData().dataList[j].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
									detailModel.getData().dataList[j].Vadet);
								costs.push({
									pb: detailModel.getData().dataList[j].Waers,
									cost: parseFloat(detailModel.getData().dataList[j].Wrbtr)
								});
							}
							view.setModel(detailModel, "formnDetailData");
							that.findTotalCost(detailModel, costs, view);
							sap.ui.getCore().byId("detailLifnr").setVisible(false);
							sap.ui.getCore().byId("detailName1").setVisible(false);
							sap.ui.getCore().byId("detailOdmtu").setVisible(false);
							sap.ui.getCore().byId("detailBpbak").setVisible(false);
							sap.ui.getCore().byId("detailUpbak").setVisible(false);
							sap.ui.getCore().byId("detailVdbak").setVisible(false);
							sap.ui.getCore().byId("detailWaers").setVisible(false);
						}
					}
				}
			});
		},
		
		detailLifnr: function (param, e) {
			var that = this;
			// var oRow = e.getParameter("row");
			// var index = oRow.getIndex();

			// // var table = param.table;
			// var table = sap.ui.getCore().byId("detailTable");
			var Lifnr = e.getSource().getText();
			var Odmfn = param.Odmfn;
			var model = new JSONModel();
			var costs = [];
			var view = param.view;
			model.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OdmfnDetaySet?$filter=Odmfn eq '" + Odmfn + "' and Lifnr eq '" + Lifnr +
				"' and Level eq '2'");
			model.attachRequestCompleted(function onComplete(oEvent) {
				sap.ui.core.BusyIndicator.show();
				if (oEvent.getParameter("success")) {
					var data = oEvent.getSource().getData().d.results;
					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					view.setModel(newModel, "detailLifnr");

					for (var j = 0; j < data.length; j++) {
						costs.push({
							pb: newModel.getData().dataList[j].Waers,
							cost: parseFloat(newModel.getData().dataList[j].Wrbtr)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});
					var costModel = new JSONModel(result);
					costModel.setProperty("/dataList", result);
					view.setModel(costModel, "costDataDetail");

					// that.findTotalCost(newModel, costs, view);

				}
				sap.ui.core.BusyIndicator.hide();
			});
			if (!that.oDialog3) {
				that.oDialog3 = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.OdmfnLifnrDetail", this);
				view.addDependent(that.oDialog3);
			}
			that.oDialog3.removeAllButtons();
			that.oDialog3.addButton(new sap.m.Button({
				text: "Geri",
				press: function () {
					that.oDialog3.close();
					that.oDialog3.destroy();
					that.oDialog3 = null;
				}
			}));
			that.oDialog3.open();
		},

		findTotalCost: function (model, cost, view) {
			var result = [];
			cost.reduce(function (res, value) {
				if (!res[value.pb]) {
					res[value.pb] = {
						pb: value.pb,
						cost: 0
					};
					result.push(res[value.pb]);
				}
				res[value.pb].cost += value.cost;
				return res;
			}, {});
			var costModel = new JSONModel(result);
			costModel.setProperty("/dataList", result);
			view.setModel(costModel, "costDataDetail");
		},

		openPopover: function (e) {
			var table = this.getView().byId("OnayBekTalepTable");
			this.showDetailOdemeFormn(e, table);
		},
		DetailApprover: function (e, Odmfn) {
			var that = this;
			sap.ui.core.BusyIndicator.show();
			var oButton = e.getSource();
			if (!this._oQuickView) {
				Fragment.load({
					name: "MDP_E-Odeme.FIEodeme.view.detailApprover",
					controller: this
				}).then(function (oQuickView) {
					var approverModel = new JSONModel();
					approverModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/AnBelOnayDurumSet?$filter=Odmfn eq '" + Odmfn + "'");
					approverModel.attachRequestCompleted(function onComplete(oEvent) {
						if (oEvent.getParameter("success")) {
							var data = oEvent.getSource().getData().d.results;
							for (var i = 0; i < data.length; i++) {
								data[i].Ontrh = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
									data[i].Ontrh);
								data[i].Onsat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").Time(
									data[i].Onsat);
							}
							approverModel.setProperty("/approverList", data);
							oQuickView.setModel(approverModel, "approver");
						}
					});
					oQuickView.openBy(oButton);
					sap.ui.core.BusyIndicator.hide();
				});
			} else {
				this.getView().addDependent(this._oQuickView);
				this._oQuickView.openBy(oButton);
			}
		},
		detailApprover: function (e) {
			var table = this.getView().byId("OnayBekTalepTable");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			this.DetailApprover(e, Odmfn);
		},
		ExportExcel: function (e) {
			this.exportExcel("OnayBekTalepTable", "OnayBekTalep", "dataList", "Onayımda Bekleyen Talepler");
		},
		exportExcel: function (tableId, modelName, property, fileName) {
			var that = this;
			var view = this.getView();
			var table = view.byId(tableId); //parametre olarak tanımlanan tablonun bilgisi
			var selectedIndices = table.getSelectedIndices(); //tablodaki seçili indexler
			var data = [];
			var model = new JSONModel();
			var columns = table.getColumns(); //tablonun columnlarını alır.
			var exportColumns = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				var singleData = view.getModel(modelName).getProperty("/" + property + "/" + selectedIndices[i]);
				var obj = $.extend(true, {}, singleData); //referanstan kurtarmak için kullanılır.
				if (Object.entries(obj).length > 0) {
					data.push(obj);
				}
			}
			model.setProperty("/dataList", data);
			var path;
			for (var j = 0; j < columns.length; j++) {
				if (columns[j].getTemplate().getBinding("text")) { // tablodaki veriler ve yolları geliyor.
					path = columns[j].getTemplate().getBinding("text").parts[0].path; //columndaki bilgi geliyor.Cari Hesap vs.
					// var path = columns[j].getLabel().getText();
				}
				// var path = columns[j].getLabel().getText();
				// var path = columns[j].getTemplate().mBindingInfos.text.parts[0].path;
				else {
					path = columns[j].getTemplate().getBinding("text");
				}
				var temp = {
					name: columns[j].getLabel().getText(),
					template: {
						content: "{" + path + "}"
					}
				};
				exportColumns.push(temp);
			}
			var oExport = new Export({
				exportType: new ExportTypeCSV({
					fileExtension: "csv",
					separatorChar: ";"
				}),
				models: model,
				rows: {
					path: "/dataList"
				},
				columns: exportColumns
			});
			oExport.saveFile(fileName).catch(function (error) {}).then(function () {
				oExport.destroy();
			});
		},
		// changeColor: function (e) {
		// 	var table = this.getView().byId("OnayBekTalepTable");
		// 	var index = table.getSelectedIndex();
		// 	if (index === -1) {
		// 		table.getRows()[index].removeStyleClass("bgColor");
		// 		table.getRows()[index].addStyleClass("bgColorWhite");
		// 	} else {
		// 		table.getRows()[index].addStyleClass("bgColor");
		// 		table.getRows()[index].removeStyleClass("bgColorWhite");
		// 	}
		// 	// var index = e.getSource().oParent.getIndex()
		// 	// if (table.getRows()[index].getCells()[8].getSelected()) {
		// 	// 	table.getRows()[index].removeStyleClass("bgColorWhite");
		// 	// 	table.getRows()[index].addStyleClass("bgColorRed");
		// 	// } else {
		// 	// 	table.getRows()[index].removeStyleClass("bgColorRed");
		// 	// 	table.getRows()[index].addStyleClass("bgColorWhite");
		// 	// }
		// }
	});
});