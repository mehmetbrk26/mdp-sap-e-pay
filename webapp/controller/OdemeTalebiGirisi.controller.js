sap.ui.define([
	"jquery.sap.global",
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/FilterType",
	"sap/m/Popover"
], function (jQuery, Controller, JSONModel, MessageBox, Filter, FilterOperator, FilterType, Popover) {
	"use strict";

	var globalSelectedCompanyCode = 0;
	var globalSelectedPaymentType = 0,
		globalOneriList, globalAcikList;
	var inputId, valueHelpDialog, globalAccountCodeKey, globalAccountCodeTitle;
	var inputIdD, valueHelpDialogD, globalAccountCodeKeyD, globalAccountCodeTitleD;
	var globalPaymentTypeFlag, globalApproverData, globalPaymentAmount, globaLCurrency;
	var globalSelectedBusinessField, globalSelectedCostCenter, globalDocDate, globalBasicDate;
	var globalSelectedLifnr, globalSelectedKunnr, globalSelectedCurrency, globalChoosenAmount;

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi", {

		onInit: function () {
			var that = this;
			that.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.onAfterRendering();
				}
			});
		},

		onAfterRendering: function () {
			var that = this;
			var x = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().get("myLocalData");
			x.flagForSuggestedList = "0";
			x.flagForOpenPaymentsList = "0";
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().put("myLocalData", x);

			this.getView().byId("DocDate").setDateValue(new Date());
			this.getView().byId("BasicDate").setDateValue(new Date());
			// this.getView().byId("Currency").setEditable(true);
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + x.localUserNameSurname.toString());
			// this.getView().byId("name").setValue(x.localUserNameSurname.toString());

			var companyCodeModel = new JSONModel();
			companyCodeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShSirketKodSet");
			companyCodeModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					var companyCodeData = oEvent.getSource().getData().d.results;
					var cModel = new JSONModel(companyCodeData);
					that.getView().setModel(cModel, "companyCodeList");
				}
			});

			var currencyModel = new JSONModel();
			currencyModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShPBSet");
			currencyModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					var currencyData = oEvent.getSource().getData().d.results;
					var cModel = new JSONModel(currencyData);
					cModel.setSizeLimit(300);
					that.getView().setModel(cModel, "currencyList");
				}
			});

			var businessFieldModel = new JSONModel();
			businessFieldModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShIsAlaniSet");
			businessFieldModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					var businessFieldData = oEvent.getSource().getData().d.results;
					var cModel = new JSONModel(businessFieldData);
					that.getView().setModel(cModel, "businessFieldList");
					sap.ui.core.BusyIndicator.hide();
				}
			});

			that.getView().byId("ChoosenAmount").setValue(globalChoosenAmount);
			that.getView().byId("PaymentAmount").setValue(globalPaymentAmount);
			that.getView().byId("Currency").setValue(globaLCurrency);
			if (globalOneriList || globalAcikList) {
				that.getView().byId("Currency3").setValue(globaLCurrency);
			}
			that.getCurrencyRate();
			that.setDecimalPointsCA();
			that.setDecimalPointsPA();
		},

		getGlobalSelectedCompanyCode: function () {
			return globalSelectedCompanyCode;
		},

		getGlobalSelectedPaymentType: function () {
			return globalSelectedPaymentType;
		},

		getGlobalSelectedBusinessField: function () {
			return globalSelectedBusinessField;
		},

		// getGlobalSelectedCostCenter: function () {
		// 	return globalSelectedCostCenter;
		// },

		getGlobalDocDate: function () {
			return globalDocDate;
		},

		getGlobalBasicDate: function () {
			return globalBasicDate;
		},

		getGlobalSelectedLifnr: function () {
			return globalSelectedLifnr;
		},

		getGlobalSelectedKunnr: function () {
			return globalSelectedKunnr;
		},

		getGlobalSelectedCurrency: function () {
			return globalSelectedCurrency;
		},

		setChoosenAmount: function (digit) {
			globalChoosenAmount = digit;
		},

		setPaymentAmount: function (digit) {
			globalPaymentAmount = digit;
		},

		setCurrency: function (currency) {
			globaLCurrency = currency;
		},

		// CurrencyFormat: function (e) {
		// 	var value = e.getSource().getValue();
		// 	var oCurrencyFormat = sap.ui.core.format.NumberFormat.getCurrencyInstance({
		// 		currencyCode: false
		// 	});
		// 	if (value.includes(",")) {
		// 		value = value.replace(",", ".");
		// 	}
		// 	if (e.getSource().getSelectedKey() === "1") {
		// 		this.getView().byId("PaymentAmount").setValue(oCurrencyFormat.format(value));
		// 	} else if (e.getSource().getSelectedKey() === "2") {
		// 		this.getView().byId("ChoosenAmount").setValue(oCurrencyFormat.format(value));
		// 	}
		// },

		setDecimalPointsCA: function (oEvent) {
			var oNumberFormat = sap.ui.core.format.NumberFormat.getFloatInstance({
				maxFractionDigits: 2,
				groupingEnabled: true,
				groupingSeparator: ".",
				decimalSeparator: ","
			});
			// this.getView().byId("ChoosenAmount").setValue(oNumberFormat.format(oEvent.getSource().getValue()));
			if (oEvent !== undefined) {
				var n = -1;
				var str = oEvent.getSource().getValue();
				n = str.search(",");
				if (n !== -1) {
					str = str.replace(",", ".");
				}
				var m = -1;
				m = str.search("\\.");
				if (m === -1) {
					str = str + ".00";
				}
				this.setPaymentAmount(str);
				this.getView().byId("ChoosenAmount").setValue(oNumberFormat.format(str));
			} else if (this.getView().byId("ChoosenAmount").getValue() !== undefined) {
				var str = this.getView().byId("ChoosenAmount").getValue();
				var n = -1;
				n = str.search(",");
				if (n !== -1) {
					str = str.replace(",", ".");
				}
				var m = -1;
				m = str.search("\\.");
				if (m === -1) {
					str = str + ".00";
				}
				this.getView().byId("ChoosenAmount").setValue(oNumberFormat.format(str));
			}
		},

		setDecimalPointsPA: function (oEvent) {
			var oNumberFormat = sap.ui.core.format.NumberFormat.getFloatInstance({
				maxFractionDigits: 2,
				groupingEnabled: true,
				groupingSeparator: ".",
				decimalSeparator: ","
			});
			// this.getView().byId("PaymentAmount").setValue(oNumberFormat.format(oEvent.getSource().getValue()));
			if (oEvent !== undefined) {
				var n = -1;
				var str = oEvent.getSource().getValue();
				n = str.search(",");
				if (n !== -1) {
					str = str.replace(",", ".");
				}
				var m = -1;
				m = str.search("\\.");
				if (m === -1) {
					str = str + ".00";
				}
				this.setPaymentAmount(str);
				this.getView().byId("PaymentAmount").setValue(oNumberFormat.format(str));
			} else if (this.getView().byId("PaymentAmount").getValue() !== undefined) {
				var str = this.getView().byId("PaymentAmount").getValue();
				var n = -1;
				n = str.search(",");
				if (n !== -1) {
					str = str.replace(",", ".");
				}
				var m = -1;
				m = str.search("\\.");
				if (m === -1) {
					str = str + ".00";
				}
				this.getView().byId("PaymentAmount").setValue(oNumberFormat.format(str));
			}
		},

		goHome: function (oEvent) {
			this.clearAllFields();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							that.clearAllFields();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiOneriListesiDetayi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Oneri_Listesi_Detayi.
								_oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiOneriListesiDetayi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		showApproverandSave: function (e) {
			// sap.ui.core.BusyIndicator.show();
			var that = this;
			if (!globalPaymentAmount || globalPaymentAmount === undefined || globalPaymentAmount === null) {
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
				MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
				sap.ui.core.BusyIndicator.hide();
			} else if (!globalPaymentAmount.toString().match(/^-?\d*(\.\d+)?$/)) {
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
				MessageBox.alert("Ödeme Tutarı alanına sadece sayısal değer girilebilir!");
				sap.ui.core.BusyIndicator.hide();
			} else if (this.getView().byId("CompanyCode").getValue() === undefined || this.getView().byId("PaymentAmount").getValue() ===
				undefined || this.getView().byId("Currency").getValue() === undefined) {
				if (globalOneriList) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
				} else if (this.getView().byId("accountCode").getVisible() && globalAccountCodeKey === undefined) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
					that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
					MessageBox.alert("Lütfen satıcı için cari hesap kodu alanını doldurunuz!");
				} else if (this.getView().byId("accountCodeD").getVisible() && globalAccountCodeKeyD === undefined) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
					that.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.Error);
					MessageBox.alert("Lütfen müşteri için cari hesap kodu alanını doldurunuz!");
				}
				sap.ui.core.BusyIndicator.hide();
			} else {
				var localWrbtr = parseFloat(globalPaymentAmount, 10);
				var localWaers = this.getView().byId("Currency").getValue();
				var approverModel = new JSONModel();
				approverModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ApproversSet?$filter=Pernr eq '" + sap.ui.controller(
						"MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "' and  Bukrs eq '" + globalSelectedCompanyCode +
					"' and Wrbtr eq " + localWrbtr + " and Waers eq '" + localWaers + "'");
				approverModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("sf3").setVisible(true);
						globalApproverData = oEvent.getSource().getData().d.results;
						// sap.ui.core.BusyIndicator.hide();
						var cModel = new JSONModel(globalApproverData);
						that.getView().setModel(cModel, "approverList");
					}
				});
				// this.getView().byId("Register").setEnabled(true);
				this.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.None);
				this.registerPaymentRequest();
			}
			// that.determineAccountCodes();
		},

		getRealtedDatasViaCompanyCode: function (oEvent) {
			var that = this;
			// sap.ui.core.BusyIndicator.show();
			globalSelectedCompanyCode = oEvent.getSource().getSelectedKey();
			var paymentTypeModel = new JSONModel();
			paymentTypeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShOdemeTurSet?$filter=Bukrs eq '" + globalSelectedCompanyCode + "'");
			paymentTypeModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					// sap.ui.core.BusyIndicator.hide();
					var paymentTypeData = oEvent.getSource().getData().d.results;
					var cModel = new JSONModel(paymentTypeData);
					that.getView().setModel(cModel, "paymentTypeList");
				}
			});
		},

		determineAccountCodes: function (oEvent) {
			var that = this;
			// sap.ui.core.BusyIndicator.show();
			globalSelectedPaymentType = oEvent.getSource().getSelectedKey();

			var paymentTypeModel = new JSONModel();
			paymentTypeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShOdemeTurSet?$filter=Bukrs eq '" + globalSelectedCompanyCode +
				"' and Odmtp eq '" + globalSelectedPaymentType + "'");
			paymentTypeModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					// sap.ui.core.BusyIndicator.hide();
					var paymentTypeData = oEvent.getSource().getData().d.results;
					if (paymentTypeData[0].Koart == "K") {
						that.getView().byId("accountCode").setVisible(true);
						that.getView().byId("accountCodeD").setVisible(false);
					} else if (paymentTypeData[0].Koart == "D") {
						that.getView().byId("accountCode").setVisible(false);
						that.getView().byId("accountCodeD").setVisible(true);
					}
					if (!paymentTypeData[0].Akals && !paymentTypeData[0].Tukon) {
						that.getView().byId("ChoosenAmount").setEditable(false);
						that.getView().byId("PaymentAmount").setEditable(true);
						that.getView().byId("Currency").setEditable(true);
					}
					if (paymentTypeData[0].Akals && !paymentTypeData[0].Tukon) {
						that.getView().byId("ChoosenAmount").setEditable(false);
						that.getView().byId("PaymentAmount").setEditable(true);
						that.getView().byId("Currency").setEditable(true);
					}
					if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
						that.getView().byId("ChoosenAmount").setEditable(false);
						that.getView().byId("PaymentAmount").setEditable(false);
						that.getView().byId("Currency").setEditable(true);
					}
					if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
						globalPaymentTypeFlag = true;
					} else if (paymentTypeData[0].Akals && !paymentTypeData[0].Tukon) {
						globalPaymentTypeFlag = false;
					} else {
						globalPaymentTypeFlag = null;
					}
					if (paymentTypeData[0].Onlst) {
						that.getView().byId("DisplayDetail").setText("Öneri Listesinden Seç");
						that.getView().byId("DisplayDetail").setVisible(true);
						that.getView().byId("ChoosenAmount").setEditable(false);
						that.getView().byId("PaymentAmount").setEditable(false);
						that.getView().byId("accountCode").setEditable(false);
						that.getView().byId("accountCodeD").setEditable(false);
						that.getView().byId("Currency").setEditable(false);
					} else if (paymentTypeData[0].Akals) {
						that.getView().byId("DisplayDetail").setText("Açık Kalemlerden Seç");
						that.getView().byId("DisplayDetail").setVisible(true);
						that.getView().byId("accountCode").setEditable(true);
						that.getView().byId("accountCodeD").setEditable(true);
					} else {
						that.getView().byId("DisplayDetail").setText("Ödeme Detayı Görüntüle");
						that.getView().byId("DisplayDetail").setVisible(false);
						that.getView().byId("accountCode").setEditable(true);
						that.getView().byId("accountCodeD").setEditable(true);
					}
				}
			});
			that.getView().byId("accountCode").setValue("");
			that.getView().byId("accountCodeD").setValue("");
			that.getView().byId("ChoosenAmount").setValue("");
			that.getView().byId("PaymentAmount").setValue("");
			that.getView().byId("Currency").setValue("");
			that.getView().byId("Currency3").setValue("");
			that.getView().byId("CurrencyRate").setValue("");
			that.getView().byId("PaymentText").setValue("");
			that.getView().byId("BankExplanation").setValue("");
			globalApproverData = [];
			var cModel = new JSONModel(globalApproverData);
			that.getView().setModel(cModel, "approverList");
			inputId = null;
			valueHelpDialog = null;
			globalAccountCodeKey = null;
			globalAccountCodeTitle = null;
			inputIdD = null;
			valueHelpDialogD = null;
			globalAccountCodeKeyD = null;
			globalAccountCodeTitleD = null;
			globalChoosenAmount = null;
			globalPaymentTypeFlag = null;
			globalPaymentAmount = null;
			globaLCurrency = null;
			that.getView().byId("DisplayDetail").setVisible(false);
			that.getView().byId("DisplayDetail").setIcon(null);
			that.getView().byId("DisplayDetail").setType(null);
			that.getView().byId("sf3").setVisible(false);
		},

		getGlobalPaymentTypeFlag: function () {
			return globalPaymentTypeFlag;
		},

		clearAllFields: function (oView) {
			// oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined.byId("DocDate")
			var that = this.getView();
			try {
				if (oView && oView.getViewName()) {
					that = oView;
				}
			} catch (err) {
				that = this.getView();
			}
			that.byId("CompanyCode").setValue("");
			that.byId("CompanyCode").setSelectedKey("");
			that.byId("DocDate").setDateValue(new Date());
			that.byId("BasicDate").setDateValue(new Date());
			that.byId("PaymentType").setValue("");
			that.byId("PaymentType").setSelectedKey("");
			that.byId("accountCode").setValue("");
			that.byId("accountCodeD").setValue("");
			that.byId("ChoosenAmount").setValue("");
			that.byId("PaymentAmount").setValue("");
			that.byId("Currency").setValue("");
			that.byId("Currency").setSelectedKey("");
			that.byId("Currency3").setValue("");
			that.byId("CurrencyRate").setValue("");
			that.byId("PaymentText").setValue("");
			that.byId("BankExplanation").setValue("");
			that.byId("BusinessField").setValue("");
			that.byId("BusinessField").setSelectedKey("");
			// that.getView().byId("CostCenter").setValue("");
			// that.getView().byId("CostCenter").setSelectedKey("");
			globalApproverData = [];
			var cModel = new JSONModel(globalApproverData);
			sap.ui.getCore().setModel(cModel, "approverList");
			globalSelectedCompanyCode = null;
			globalSelectedPaymentType = null;
			inputId = null;
			valueHelpDialog = null;
			globalAccountCodeKey = null;
			globalAccountCodeTitle = null;
			inputIdD = null;
			valueHelpDialogD = null;
			globalAccountCodeKeyD = null;
			globalAccountCodeTitleD = null;
			globalChoosenAmount = null;
			globalPaymentTypeFlag = null;
			globalPaymentAmount = null;
			globaLCurrency = null;
			that.byId("DisplayDetail").setVisible(false);
			that.byId("DisplayDetail").setIcon(null);
			that.byId("DisplayDetail").setType(null);
			that.byId("sf3").setVisible(false);
		},

		goToTargetPaymentRequestsDetails: function (oEvent) {
			// sap.ui.core.BusyIndicator.show();
			var that = this;
			var paymentTypeModel = new JSONModel();
			that.getView().byId("PaymentAmount").setValue("");
			that.getView().byId("ChoosenAmount").setValue("");
			that.getView().byId("Currency3").setValue("");
			that.getView().byId("CurrencyRate").setValue("");
			paymentTypeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShOdemeTurSet?$filter=Bukrs eq '" + globalSelectedCompanyCode + "' and Odmtp eq '" + globalSelectedPaymentType + "'");
			paymentTypeModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					// sap.ui.core.BusyIndicator.hide();
					var paymentTypeData = oEvent.getSource().getData().d.results;
					var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
					if (paymentTypeData[0].Onlst) {
						that.getView().byId("DisplayDetail").setIcon("sap-icon://accept");
						that.getView().byId("DisplayDetail").setType("Critical");
						oRouter.navTo("Target_Odeme_Talebi_Oneri_Listesi_Detayi");
						globalOneriList = true;
						globalAcikList = false;
					} else {
						if (that.getView().byId("accountCode").getVisible()) {
							if (that.getView().byId("CompanyCode").getValue() && that.getView().byId("DocDate").getValue() &&
								globalAccountCodeKey && that.getView().byId("Currency").getValue()) {
								if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
									that.getView().byId("Currency").setEditable(false);
								}
								globalSelectedCompanyCode = that.getView().byId("CompanyCode").getSelectedKey();
								globalSelectedBusinessField = that.getView().byId("BusinessField").getSelectedKey();
								// globalSelectedCostCenter = that.getView().byId("CostCenter").getSelectedKey();
								globalDocDate = that.getView().byId("DocDate").getValue();
								globalBasicDate = that.getView().byId("BasicDate").getValue();
								globalSelectedLifnr = globalAccountCodeKey;
								globalSelectedCurrency = that.getView().byId("Currency").getSelectedKey();
								that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DisplayDetail").setIcon("sap-icon://accept");
								that.getView().byId("DisplayDetail").setType("Critical");
								oRouter.navTo("Target_Odeme_Talebi_Detayi");
								globalAcikList = true;
								globalOneriList = false;
							} else if (globalPaymentTypeFlag === false && that.getView().byId("CompanyCode").getValue() && that.getView().byId("DocDate").getValue() &&
								globalAccountCodeKey) {
								if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
									that.getView().byId("Currency").setEditable(false);
								}
								globalSelectedCompanyCode = that.getView().byId("CompanyCode").getSelectedKey();
								globalSelectedBusinessField = that.getView().byId("BusinessField").getSelectedKey();
								// globalSelectedCostCenter = that.getView().byId("CostCenter").getSelectedKey();
								globalDocDate = that.getView().byId("DocDate").getValue();
								globalBasicDate = that.getView().byId("BasicDate").getValue();
								globalSelectedLifnr = globalAccountCodeKey;
								globalSelectedCurrency = that.getView().byId("Currency").getSelectedKey();
								that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DisplayDetail").setIcon("sap-icon://accept");
								that.getView().byId("DisplayDetail").setType("Critical");
								oRouter.navTo("Target_Odeme_Talebi_Detayi");
								globalAcikList = true;
								globalOneriList = false;
							} else {
								if (that.getView().byId("CompanyCode").getValue().length === 0 || that.getView().byId("CompanyCode").getValue() === undefined) {
									that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
								}
								if (that.getView().byId("DocDate").getValue().length === 0 || that.getView().byId("DocDate").getValue() === undefined) {
									that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen belge tarihi alanını doldurunuz!");
								}
								if (that.getView().byId("accountCode").getValue().length === 0 || that.getView().byId("accountCode").getValue() === undefined ||
									globalAccountCodeKey === null || globalAccountCodeKey.toString().length === 0 || globalAccountCodeKey === undefined) {
									globalAccountCodeKey = null;
									that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen satıcı için cari hesap kodu alanını doldurunuz!");
								}
								if (globalPaymentTypeFlag === true) {
									if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
										that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
										MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
									}
								}
							}
						}
						if (that.getView().byId("accountCodeD").getVisible()) {
							if (that.getView().byId("CompanyCode").getValue() && that.getView().byId("DocDate").getValue() &&
								globalAccountCodeKeyD && that.getView().byId("Currency").getValue()) {
								if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
									that.getView().byId("Currency").setEditable(false);
								}
								globalSelectedCompanyCode = that.getView().byId("CompanyCode").getSelectedKey();
								globalSelectedBusinessField = that.getView().byId("BusinessField").getSelectedKey();
								globalSelectedCostCenter = that.getView().byId("CostCenter").getSelectedKey();
								globalDocDate = that.getView().byId("DocDate").getValue();
								globalBasicDate = that.getView().byId("BasicDate").getValue();
								globalSelectedKunnr = globalAccountCodeKeyD;
								globalSelectedCurrency = that.getView().byId("Currency").getSelectedKey();
								that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DisplayDetail").setIcon("sap-icon://accept");
								that.getView().byId("DisplayDetail").setType("Critical");
								oRouter.navTo("Target_Odeme_Talebi_Detayi");
								globalAcikList = true;
								globalOneriList = false;
							} else if (globalPaymentTypeFlag === false && that.getView().byId("CompanyCode").getValue() && that.getView().byId("DocDate").getValue() &&
								globalAccountCodeKeyD) {
								if (paymentTypeData[0].Akals && paymentTypeData[0].Tukon) {
									that.getView().byId("Currency").setEditable(false);
								}
								globalSelectedCompanyCode = that.getView().byId("CompanyCode").getSelectedKey();
								globalSelectedBusinessField = that.getView().byId("BusinessField").getSelectedKey();
								globalSelectedCostCenter = that.getView().byId("CostCenter").getSelectedKey();
								globalDocDate = that.getView().byId("DocDate").getValue();
								globalBasicDate = that.getView().byId("BasicDate").getValue();
								globalSelectedKunnr = globalAccountCodeKeyD;
								globalSelectedCurrency = that.getView().byId("Currency").getSelectedKey();
								that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("DisplayDetail").setIcon("sap-icon://accept");
								that.getView().byId("DisplayDetail").setType("Critical");
								oRouter.navTo("Target_Odeme_Talebi_Detayi");
								globalAcikList = true;
								globalOneriList = false;
							} else {
								if (that.getView().byId("CompanyCode").getValue().length === 0 || that.getView().byId("CompanyCode").getValue() === undefined) {
									that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
								}
								if (that.getView().byId("DocDate").getValue().length === 0 || that.getView().byId("DocDate").getValue() === undefined) {
									that.getView().byId("DocDate").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen belge tarihi alanını doldurunuz!");
								}
								if (that.getView().byId("accountCodeD").getValue().length === 0 || that.getView().byId("accountCodeD").getValue() === undefined ||
									globalAccountCodeKeyD === null || globalAccountCodeKeyD.toString().length === 0 || globalAccountCodeKeyD === undefined) {
									globalAccountCodeKeyD = null;
									that.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.Error);
									MessageBox.alert("Lütfen müşteri için cari hesap kodu alanını doldurunuz!");
								}
								if (globalPaymentTypeFlag === true) {
									if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
										that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
										MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
									}
								}
							}
						}
					}
				}
			});
		},

		registerPaymentRequest: function (oEvent) {
			// sap.ui.core.BusyIndicator.show();
			var that = this;
			var localDocDate = that.getView().byId("DocDate").getValue();
			var localBasicDate = that.getView().byId("BasicDate").getValue();
			if (localDocDate !== undefined) {
				if (localDocDate.substring(0, 2).includes(".")) {
					localDocDate = localDocDate.substring(5, 9) + "-" + localDocDate.substring(2, 4) + "-0" + localDocDate.substring(0, 1) +
						"T00:00:00";
				} else {
					localDocDate = localDocDate.substring(6, 10) + "-" + localDocDate.substring(3, 5) + "-" + localDocDate.substring(0, 2) +
						"T00:00:00";
				}
			}
			if (localBasicDate !== undefined) {
				if (localBasicDate.substring(0, 2).includes(".")) {
					localBasicDate = localBasicDate.substring(5, 9) + "-" + localBasicDate.substring(2, 4) + "-0" + localBasicDate.substring(0, 1) +
						"T00:00:00";
				} else {
					localBasicDate = localBasicDate.substring(6, 10) + "-" + localBasicDate.substring(3, 5) + "-" + localBasicDate.substring(0, 2) +
						"T00:00:00";
				}
			}
			
			var strPaymentAmount = globalPaymentAmount.toString();
			if (that.getView().byId("DisplayDetail").getVisible()) {
				if (globalOneriList) {
					var jsonOneri = {
						"Kursf": that.getView().byId("CurrencyRate").getValue(),
						"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr(),
						"Bukrs": globalSelectedCompanyCode,
						"Bldat": localDocDate,
						"Zfbdt": localBasicDate,
						"Odmtp": that.getView().byId("PaymentType").getSelectedKey(),
						"Lifnr": "",
						"Kunnr": "",
						"Odmtu": strPaymentAmount,
						"Waers": that.getView().byId("Currency").getValue(),
						"Sgtxt": that.getView().byId("PaymentText").getValue(),
						"Bntxt": that.getView().byId("BankExplanation").getValue(),
						"Gsber": that.getView().byId("BusinessField").getSelectedKey(),
						"TalepOListesi": []
					};
					jsonOneri.TalepOListesi = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiOneriListesiDetayi").getGlobalSelectedIndicesOneriList();
					that.createModel = new JSONModel(jsonOneri);
				} else if (globalAcikList) {
					var jsonAcik = {
						"Kursf": that.getView().byId("CurrencyRate").getValue(),
						"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr(),
						"Bukrs": globalSelectedCompanyCode,
						"Bldat": localDocDate,
						"Zfbdt": localBasicDate,
						"Odmtp": that.getView().byId("PaymentType").getSelectedKey(),
						"Lifnr": globalAccountCodeKey,
						"Kunnr": globalAccountCodeKeyD,
						"Odmtu": strPaymentAmount,
						"Waers": that.getView().byId("Currency").getValue(),
						"Sgtxt": that.getView().byId("PaymentText").getValue(),
						"Bntxt": that.getView().byId("BankExplanation").getValue(),
						"Gsber": that.getView().byId("BusinessField").getSelectedKey(),
						"TalepAKalem": []
					};
					jsonAcik.TalepAKalem = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiDetayi").getGlobalSelectedIndicesAcikKalemList();
					that.createModel = new JSONModel(jsonAcik);
				}
			} else {
				var json = {
					"Kursf": that.getView().byId("CurrencyRate").getValue(),
					"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr(),
					"Bukrs": globalSelectedCompanyCode,
					"Bldat": localDocDate,
					"Zfbdt": localBasicDate,
					"Odmtp": that.getView().byId("PaymentType").getSelectedKey(),
					"Lifnr": globalAccountCodeKey,
					"Kunnr": globalAccountCodeKeyD,
					"Odmtu": strPaymentAmount,
					"Waers": that.getView().byId("Currency").getValue(),
					"Sgtxt": that.getView().byId("PaymentText").getValue(),
					"Bntxt": that.getView().byId("BankExplanation").getValue(),
					"Gsber": that.getView().byId("BusinessField").getSelectedKey(),
					"TalepOListesi": [],
					"TalepAKalem": []
				};
				that.createModel = new JSONModel(json);
			}

			that.getView().setModel(that.createModel, "createCollection");
			var oSOData = this.getView().getModel("createCollection").getData();
			var oModel = this.getOwnerComponent().getModel();
			oModel.create("/OdemeTalebiSet", oSOData, {
				method: "POST",
				header: "Content-Type: application/json",
				success: function (scc) {
					that.getView().byId("PaymentAmount").setValue(globalPaymentAmount);
					that.setDecimalPointsPA();
					sap.ui.core.BusyIndicator.hide();
					MessageBox.success(scc.Odmfn + " " + "numaralı form başarılı bir şekilde oluşturuldu.", {
						icon: MessageBox.Icon.INFORMATION,
						actions: [MessageBox.Action.OK],
						emphasizedAction: MessageBox.Action.OK,
						initialFocus: MessageBox.Action.CANCEL,
						onClose: function (sButton) {
							if (sButton === MessageBox.Action.OK) {
								that.clearAllFields(oEvent);
								// myLocation.reload();
							}
						}
					});
					oModel.refresh();
					// sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiDetayi").setGlobalSaveAndReturnFlag(0);
				},
				error: function (err) {
					sap.ui.core.BusyIndicator.hide();
					that.getView().byId("PaymentAmount").setValue(globalPaymentAmount);
					that.setDecimalPointsPA();
					var params = err.responseText;
					MessageBox.error(JSON.parse(params).error.innererror.errordetails[0].message, {
						// MessageBox.error("Uygun banka verisi bulunamadı!", {
						icon: MessageBox.Icon.ERROR,
						actions: [MessageBox.Action.OK, MessageBox.Action.CANCEL],
						emphasizedAction: MessageBox.Action.OK,
						initialFocus: MessageBox.Action.CANCEL,
						onClose: function (sButton) {
							if (sButton === MessageBox.Action.CANCEL) {
								// that.clearAllFields(oEvent);
							}
						}
					});
				}
			});
			that.getView().byId("PaymentAmount").setValue(globalPaymentAmount);
			that.setDecimalPointsPA();
		},

		getCurrencyRate: function (oEvent) {
			var that = this;
			var localCurrency = that.getView().byId("Currency").getSelectedKey();
			// that.getView().byId("Currency3").setValue(that.getView().byId("Currency").getSelectedKey().toString());
			if (localCurrency === "") {
				localCurrency = that.getView().byId("Currency").getValue();
				// that.getView().byId("Currency3").setValue(that.getView().byId("Currency").getValue().toString());
			}
			if (localCurrency !== "") {
				var currencyModel = new JSONModel();
				currencyModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShPBSet");
				currencyModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var currencyData = oEvent.getSource().getData().d.results;
						for (var i = 0; i < currencyData.length; i++) {
							if (currencyData[i].Waers == localCurrency) {
								that.getView().byId("CurrencyRate").setValue(currencyData[i].Kursf);
							}
						}
					}
				});
			}
		},

		getApproverList: function (oEvent) {
			var that = this;
			if (globalPaymentAmount === undefined) {
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
				MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
				// sap.ui.core.BusyIndicator.hide();
			} else if (!globalPaymentAmount.toString().match(/^-?\d*(\.\d+)?$/)) {
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
				MessageBox.alert("Ödeme Tutarı alanına sadece sayısal değer girilebilir!");
				// sap.ui.core.BusyIndicator.hide();
			} else if (this.getView().byId("CompanyCode").getValue() === undefined || this.getView().byId("PaymentAmount").getValue() ===
				undefined || this.getView().byId("Currency").getValue() === undefined) {
				if (globalOneriList) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
				} else if (this.getView().byId("accountCode").getVisible() && globalAccountCodeKey === undefined) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
					that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
					MessageBox.alert("Lütfen satıcı için cari hesap kodu alanını doldurunuz!");
				} else if (this.getView().byId("accountCodeD").getVisible() && globalAccountCodeKeyD === undefined) {
					if (that.getView().byId("CompanyCode").getValue() === undefined) {
						that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen şirket kodu alanını doldurunuz!");
					}
					if (that.getView().byId("PaymentAmount").getValue() === undefined) {
						that.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.Error);
						MessageBox.alert("Lütfen ödeme tutarı alanını doldurunuz!");
					}
					if (globalPaymentTypeFlag === true) {
						if (that.getView().byId("Currency").getValue().length === 0 || that.getView().byId("Currency").getValue() === undefined) {
							that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
							MessageBox.alert("Lütfen para birimi alanını doldurunuz!");
						}
					}
					that.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.Error);
					MessageBox.alert("Lütfen müşteri için cari hesap kodu alanını doldurunuz!");
				}
				// sap.ui.core.BusyIndicator.hide();
			} else {
				var localWrbtr = parseFloat(globalPaymentAmount, 10);
				var localWaers = this.getView().byId("Currency").getValue();
				var approverModel = new JSONModel();
				approverModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ApproversSet?$filter=Pernr eq '" + sap.ui.controller(
						"MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "' and  Bukrs eq '" + globalSelectedCompanyCode +
					"' and Wrbtr eq " + localWrbtr + " and Waers eq '" + localWaers + "'");
				approverModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("sf3").setVisible(true);
						globalApproverData = oEvent.getSource().getData().d.results;
						// sap.ui.core.BusyIndicator.hide();
						var cModel = new JSONModel(globalApproverData);
						that.getView().setModel(cModel, "approverList");
					}
				});
				this.getView().byId("Register").setEnabled(true);
				this.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("PaymentAmount").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
				this.getView().byId("accountCodeD").setValueState(sap.ui.core.ValueState.None);
			}

		},

		handleValueHelp: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			this.inputId = oEvent.getSource().getId();
			// create value help dialog
			if (!this.valueHelpDialog) {
				this.valueHelpDialog = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.AccountCodeDialog", this);
				this.getView().addDependent(this.valueHelpDialog);
			}

			if (sInputValue.length > 4) {
				// create a filter for the binding
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Lifnr", sap.ui.model.FilterOperator.Contains, sInputValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sInputValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				this.valueHelpDialog.getBinding("items").filter(secondFilter);
			}

			// open value help dialog filtered by the input value
			this.valueHelpDialog.open(sInputValue);
		},

		handleValueHelpSearch: function (evt) {
			var sValue = evt.getParameter("value");
			if (sValue.length > 4) {
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Lifnr", sap.ui.model.FilterOperator.Contains, sValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				evt.getSource().getBinding("items").filter(secondFilter);
			}
		},

		handleValueHelpClose: function (evt) {
			var that = this;
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				globalAccountCodeKey = oSelectedItem.getBindingInfo("title").binding.aOriginalValues[0];
				globalAccountCodeTitle = evt.getParameters().selectedItem.getTitle();
				globalAccountCodeKeyD = "";
				globalAccountCodeTitleD = "";
				that.getView().byId("accountCode").setValue(globalAccountCodeTitle);
			}
			// evt.getSource().getBinding("items").filter([]);
		},

		suggestionItemSelected: function (evt) {

			var oItem = evt.getParameter('selectedItem'),
				oText = this.byId('selectedKey'),
				sKey = oItem ? oItem.getKey() : '';

			oText.setText(sKey);
		},

		handleValueHelpD: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			this.inputIdD = oEvent.getSource().getId();
			// create value help dialog
			if (!this.valueHelpDialogD) {
				this.valueHelpDialogD = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.AccountCodeDialogD", this);
				this.getView().addDependent(this.valueHelpDialogD);
			}

			if (sInputValue.length > 4) {
				// create a filter for the binding
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Kunnr", sap.ui.model.FilterOperator.Contains, sInputValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sInputValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				this.valueHelpDialogD.getBinding("items").filter(secondFilter);
			}

			// open value help dialog filtered by the input value
			this.valueHelpDialogD.open(sInputValue);
		},

		handleValueHelpSearchD: function (evt) {
			var sValue = evt.getParameter("value");
			if (sValue.length > 4) {
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Kunnr", sap.ui.model.FilterOperator.Contains, sValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				evt.getSource().getBinding("items").filter(secondFilter);
			}
		},

		handleValueHelpCloseD: function (evt) {
			var that = this;
			var oSelectedItem = evt.getParameter("selectedItem");
			if (oSelectedItem) {
				globalAccountCodeKey = "";
				globalAccountCodeTitle = "";
				globalAccountCodeKeyD = oSelectedItem.getBindingInfo("title").binding.aOriginalValues[0];
				globalAccountCodeTitleD = evt.getParameters().selectedItem.getTitle();
				that.getView().byId("accountCodeD").setValue(globalAccountCodeTitleD);
			}
			// evt.getSource().getBinding("items").filter([]);
		},

		suggestionItemSelectedD: function (evt) {

			var oItem = evt.getParameter('selectedItem'),
				oText = this.byId('selectedKey'),
				sKey = oItem ? oItem.getKey() : '';

			oText.setText(sKey);
		}

	});
});