sap.ui.define([
	'jquery.sap.global',
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (jQuery, Controller, JSONModel, MessageBox) {
	"use strict";

	var globalInitFlag = 0;
	var globalSelectedIndices = [];
	var globalAcikKalemlerDetayData;

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiDetayi", {

		onInit: function () {
			var that = this;
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.onAfterRendering();
				}
			});
		},

		onAfterRendering: function () {
			var that = this;
			that.getView().byId("acikKalemlerListesi").setBusy(true);
			var table = this.getView().byId("acikKalemlerListesi");
			var localPaymentTypeFlag = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalPaymentTypeFlag();
			if (localPaymentTypeFlag) {
				this.getView().byId("checkBoxFull").setSelected(true);
				this.getView().byId("checkBoxPartially").setSelected(false);
			} else {
				this.getView().byId("checkBoxFull").setSelected(false);
				this.getView().byId("checkBoxPartially").setSelected(true);
			}
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().get("myLocalData").flagForOpenPaymentsList ===
				"1") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
				oRouter.navTo("Target_Odeme_Talebi_Girisi");
			} else {
				// sap.ui.core.BusyIndicator.show();
				this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
				var localSelectedCompanyCode = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedCompanyCode();
				var localSelectedBusinessField = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedBusinessField();
				// var localSelectedCostCenter = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedCostCenter();
				var localDocDate = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalDocDate();
				if (localDocDate !== undefined) {
					if (localDocDate.substring(0, 2).includes(".")) {
						localDocDate = localDocDate.substring(5, 9) + "-" + localDocDate.substring(2, 4) + "-0" + localDocDate.substring(0, 1) +
							"T00:00:00";
					} else {
						localDocDate = localDocDate.substring(6, 10) + "-" + localDocDate.substring(3, 5) + "-" + localDocDate.substring(0, 2) +
							"T00:00:00";
					}
				}
				var localBasicDate = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalBasicDate();
				if (localBasicDate !== undefined) {
					if (localBasicDate.substring(0, 2).includes(".")) {
						localBasicDate = localBasicDate.substring(5, 9) + "-" + localBasicDate.substring(2, 4) + "-0" + localBasicDate.substring(0, 1) +
							"T00:00:00";
					} else {
						localBasicDate = localBasicDate.substring(6, 10) + "-" + localBasicDate.substring(3, 5) + "-" + localBasicDate.substring(0, 2) +
							"T00:00:00";
					}
				}
				var localSelectedLifnr = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedLifnr();
				var localSelectedKunnr = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedKunnr();
				var localSelectedCurrency = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedCurrency();

				var acikKalemlerDetayModel = new JSONModel();
				if (localSelectedLifnr !== undefined) {
					acikKalemlerDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayAcikKalemlerSet?$filter=Bukrs eq '" +
						localSelectedCompanyCode +
						"' and Lifnr eq '" + localSelectedLifnr + "' and KeyDate eq datetime'" + localDocDate +
						"' and Bldat eq datetime'" + localDocDate + "' and Bpbpb eq '" + localSelectedCurrency + "'");
					acikKalemlerDetayModel.attachRequestCompleted(function onComplete(oEvent) {
						if (oEvent.getParameter("success")) {
							globalAcikKalemlerDetayData = oEvent.getSource().getData().d.results;
							if (globalAcikKalemlerDetayData.length === 0) {
								sap.ui.core.BusyIndicator.hide();
								that.getView().byId("acikKalemlerListesi").setBusy(false);
								MessageBox.error(localSelectedLifnr + " " + "numaralı cari hesap için açık kalem verisi bulunamadı!", {
									icon: MessageBox.Icon.ERROR,
									actions: [MessageBox.Action.OK],
									emphasizedAction: MessageBox.Action.OK,
									// initialFocus: MessageBox.Action.CANCEL,
									onClose: function (sButton) {
										if (sButton === MessageBox.Action.OK) {
											var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
											oRouter.navTo("Target_Odeme_Talebi_Girisi");
										}
									}
								});
							} else {
								for (var i = 0; i < globalAcikKalemlerDetayData.length; i++) {
									globalAcikKalemlerDetayData[i].Bldat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Bldat);
									globalAcikKalemlerDetayData[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Vadet);
									globalAcikKalemlerDetayData[i].Rvadt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Rvadt);
								}
								table.setVisibleRowCountMode("Fixed");
								// var x = window.innerHeight - 213;
								// var count = Math.floor(x / 45);
								table.setVisibleRowCount(20);
								// table.setVisibleRowCountMode("Fixed");
								// table.setVisibleRowCount(globalAcikKalemlerDetayData.length + 1);
								var cModel = new JSONModel(globalAcikKalemlerDetayData);
								that.getView().setModel(cModel, "acikKalemlerDetayList");
								sap.ui.core.BusyIndicator.hide();
								that.getView().byId("acikKalemlerListesi").setBusy(false);
							}
						}
					});
				}
				if (localSelectedKunnr !== undefined) {
					acikKalemlerDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayAcikKalemlerSet?$filter=Bukrs eq '" +
						localSelectedCompanyCode +
						"' and Kunnr eq '" + localSelectedKunnr + "' and KeyDate eq datetime'" + localDocDate +
						"' and Bldat eq datetime'" + localDocDate + "' and Bpbpb eq '" + localSelectedCurrency + "'");
					acikKalemlerDetayModel.attachRequestCompleted(function onComplete(oEvent) {
						if (oEvent.getParameter("success")) {
							globalAcikKalemlerDetayData = oEvent.getSource().getData().d.results;
							if (globalAcikKalemlerDetayData.length === 0) {
								sap.ui.core.BusyIndicator.hide();
								that.getView().byId("acikKalemlerListesi").setBusy(false);
								MessageBox.error(localSelectedKunnr + " " + "numaralı cari hesap için açık kalem verisi bulunamadı!", {
									icon: MessageBox.Icon.ERROR,
									actions: [MessageBox.Action.OK],
									emphasizedAction: MessageBox.Action.OK,
									// initialFocus: MessageBox.Action.CANCEL,
									onClose: function (sButton) {
										if (sButton === MessageBox.Action.OK) {
											var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
											oRouter.navTo("Target_Odeme_Talebi_Girisi");
										}
									}
								});
							} else {
								for (var i = 0; i < globalAcikKalemlerDetayData.length; i++) {
									globalAcikKalemlerDetayData[i].Bldat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Bldat);
									globalAcikKalemlerDetayData[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Vadet);
									globalAcikKalemlerDetayData[i].Rvadt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
										globalAcikKalemlerDetayData[i].Rvadt);
								}
								table.setVisibleRowCountMode("Fixed");
								table.setVisibleRowCount(globalAcikKalemlerDetayData.length + 1);
								var cModel = new JSONModel(globalAcikKalemlerDetayData);
								that.getView().setModel(cModel, "acikKalemlerDetayList");
								sap.ui.core.BusyIndicator.hide();
								that.getView().byId("acikKalemlerListesi").setBusy(false);
							}
						}
					});
				}

				if (globalInitFlag === 1) {
					var x = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().get("myLocalData");
					x.flagForOpenPaymentsList = "1";
					sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().put("myLocalData", x);
				}
				globalInitFlag = 1;
			}
			// }
		},

		unchekFullPayment: function (oEvent) {
			var that = this;
			that.getView().byId("checkBoxFull").setSelected(false);
		},

		unchekPartiallyPayment: function (oEvent) {
			var that = this;
			that.getView().byId("checkBoxPartially").setSelected(false);
		},

		goPaymentEntryPage: function (oEvent) {
			this.clearAllFields();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Odeme_Talebi_Girisi");
		},

		clearAllFields: function (oEvent) {
			var that = this;
			that.getView().byId("checkBoxFull").setSelected(false);
			that.getView().byId("checkBoxPartially").setSelected(false);
			globalAcikKalemlerDetayData = [];
			var cModel = new JSONModel(globalAcikKalemlerDetayData);
			that.getView().setModel(cModel, "acikKalemlerDetayList");
		},

		// testKaydet: function(e) {
		// 	var model = sap.ui.getCore().getModel("test");
		// 	if (model) {
		// 		var data = model.getProperty("/Data");
		// 		// console.log(data);

		// 	}
		// }

		saveAndReturn: function (oEvent) {
			var totalBpbtu = 0;
			globalSelectedIndices = [];
			var table = this.getView().byId("acikKalemlerListesi");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("acikKalemlerDetayList");

			for (var i = 0; i < selectedIndices.length; i++) {
				var tmpRow = {
					"Gjahr": "",
					"Belnr": "",
					"Bukrs": "",
					"Buzei": "",
					"Chkbx": "",
					"KeyDate": "",
					"Kunnr": "",
					"Lifnr": "",
					"Name1": "",
					"Bldat": "",
					"Xblnr": "",
					"Blart": "",
					"Gsber": "",
					"Bpbtu": "",
					"Bpbpb": "",
					"Upbtu": "",
					"Upbpb": "",
					"Itmtx": "",
					"Vadet": "",
					"Rvadt": "",
					"Usnam": "",
					"Xref2": ""
				};
				tmpRow.Gjahr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Gjahr;
				tmpRow.Belnr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Belnr;
				tmpRow.Bukrs = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bukrs;
				tmpRow.Buzei = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Buzei;
				tmpRow.Chkbx = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Chkbx;
				tmpRow.KeyDate = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(model.getData()[table.getBinding(
						"rows")
					.aIndices[selectedIndices[i]]].KeyDate);
				if (tmpRow.KeyDate !== undefined) {
					if (tmpRow.KeyDate.substring(0, 2).includes(".")) {
						tmpRow.KeyDate = tmpRow.KeyDate.substring(5, 9) + "-" + tmpRow.KeyDate.substring(2, 4) + "-0" + tmpRow.KeyDate.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.KeyDate = tmpRow.KeyDate.substring(6, 10) + "-" + tmpRow.KeyDate.substring(3, 5) + "-" + tmpRow.KeyDate.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Kunnr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Kunnr;
				tmpRow.Lifnr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Lifnr;
				tmpRow.Name1 = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Name1;
				tmpRow.Bldat = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bldat;
				if (tmpRow.Bldat !== undefined) {
					if (tmpRow.Bldat.substring(0, 2).includes(".")) {
						tmpRow.Bldat = tmpRow.Bldat.substring(5, 9) + "-" + tmpRow.Bldat.substring(2, 4) + "-0" + tmpRow.Bldat.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Bldat = tmpRow.Bldat.substring(6, 10) + "-" + tmpRow.Bldat.substring(3, 5) + "-" + tmpRow.Bldat.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Xblnr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Xblnr;
				tmpRow.Blart = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Blart;
				tmpRow.Gsber = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Gsber;
				tmpRow.Bpbtu = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bpbtu;
				totalBpbtu = totalBpbtu + parseFloat(model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bpbtu, 10);
				tmpRow.Bpbpb = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bpbpb;
				tmpRow.Upbtu = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Upbtu;
				tmpRow.Upbpb = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Upbpb;
				tmpRow.Itmtx = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Itmtx;
				tmpRow.Vadet = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Vadet;
				if (tmpRow.Vadet !== undefined) {
					if (tmpRow.Vadet.substring(0, 2).includes(".")) {
						tmpRow.Vadet = tmpRow.Vadet.substring(5, 9) + "-" + tmpRow.Vadet.substring(2, 4) + "-0" + tmpRow.Vadet.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Vadet = tmpRow.Vadet.substring(6, 10) + "-" + tmpRow.Vadet.substring(3, 5) + "-" + tmpRow.Vadet.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Rvadt = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Rvadt;
				if (tmpRow.Rvadt !== undefined) {
					if (tmpRow.Rvadt.substring(0, 2).includes(".")) {
						tmpRow.Rvadt = tmpRow.Rvadt.substring(5, 9) + "-" + tmpRow.Rvadt.substring(2, 4) + "-0" + tmpRow.Rvadt.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Rvadt = tmpRow.Rvadt.substring(6, 10) + "-" + tmpRow.Rvadt.substring(3, 5) + "-" + tmpRow.Rvadt.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Usnam = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Usnam;
				tmpRow.Xref2 = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Xref2;
				globalSelectedIndices[i] = tmpRow;
			}
			totalBpbtu = Math.abs(totalBpbtu);
			totalBpbtu.toFixed(2);
			if (this.getView().byId("checkBoxFull").getSelected()) {
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setChoosenAmount(totalBpbtu);
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setPaymentAmount(totalBpbtu);
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setCurrency(globalSelectedIndices[0].Bpbpb);
			}
			if (this.getView().byId("checkBoxPartially").getSelected()) {
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setChoosenAmount(totalBpbtu);
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setCurrency(globalSelectedIndices[0].Bpbpb);
			}
			this.clearAllFields();
			table.clearSelection();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Odeme_Talebi_Girisi");
			selectedIndices = [];
		},

		getGlobalSelectedIndicesAcikKalemList: function () {
			return globalSelectedIndices;
		},

		calculateTotalAmount: function () {
			this.getView().byId("choosenPaymentAmount").toggleStyleClass("choosenPaymentAmountOpenPaymentBigAndColorful");
			this.getView().byId("choosenPaymentAmount").toggleStyleClass("choosenPaymentAmountOpenPayment");
			var table = this.getView().byId("acikKalemlerListesi");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("acikKalemlerDetayList");
			var costs = [];
			for (var j = 0; j < selectedIndices.length; j++) {
				costs.push({
					pb: model.getData()[selectedIndices[j]].Bpbpb,
					cost: parseFloat(model.getData()[table.getBinding("rows").aIndices[selectedIndices[j]]].Bpbtu, 10)
				});
			}
			var result = [];
			costs.reduce(function (res, value) {
				if (!res[value.pb]) {
					res[value.pb] = {
						pb: value.pb,
						cost: 0
					};
					result.push(res[value.pb]);
				}
				res[value.pb].cost += value.cost;
				return res;
			}, {});
			var costModel = new JSONModel(result);
			costModel.setProperty("/dataList", result);
			this.getView().setModel(costModel, "costData");

			var data = model.getData();
			var controls = [];
			for (var y = 0; y < selectedIndices.length; y++) {
				controls.push(data[selectedIndices[y]].Bpbpb);
				var val = selectedIndices[y];
				if (data[selectedIndices[0]].Bpbpb !== data[selectedIndices[y]].Bpbpb) {
					MessageBox.show("Seçilen para birimleri aynı olmalıdır.");
					table.removeSelectionInterval(val, 2);
				}
			}
			
			var dataGsber = model.getData();
			var  controlsGsber = [];
			for (var y = 0; y < selectedIndices.length; y++) {
				controlsGsber.push(dataGsber[selectedIndices[y]].Gsber);
				if (dataGsber[selectedIndices[0]].Gsber !== dataGsber[selectedIndices[y]].Gsber) {
					MessageBox.show("Seçilen iş alanları aynı olmalıdır.");
					table.removeSelectionInterval(val, 2);
				}
			}
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							that.clearAllFields();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi
								._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiOneriListesiDetayi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Oneri_Listesi_Detayi.
								_oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiOneriListesiDetayi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		changeNoDataText: function (oEvent) {
			this.getView().byId("acikKalemlerListesi").setNoData("Veri Bulunamadı!");
		}

	});

});