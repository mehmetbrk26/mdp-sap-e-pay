sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/core/Fragment",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, JSONModel, MessageToast, MessageBox, Fragment, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.MuhasebeOnayiBekleyenTalepler", {

		onInit: function () {
			var that = this;
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.counter(that);
				}
			});
		},

		getData: function () {
			var that = this;
			var OnayBekTalepModel = new JSONModel();
			OnayBekTalepModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/MuhOnaySet");
			OnayBekTalepModel.attachRequestCompleted(function onComplete(oEvent) {
				sap.ui.core.BusyIndicator.show();
				if (oEvent.getParameter("success")) {
					var data = oEvent.getSource().getData().d.results;
					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					that.getView().setModel(newModel, "MuhOnayBekTalep");
					that.handleIconTabBarSelect();
					// that.getView().byId("MuhasebeOnayBekTable").setBusy(false);
				}
				sap.ui.core.BusyIndicator.hide();
			});
		},
		counter: function (e) {
			// this.getView().byId("MuhasebeOnayBekTable").setBusy(true);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var routeParam = oRouter.getRoute("Target_Dashboard")._aRoutes[0]._router._prevRoutes[0].params[0];
			this.getView().byId("IconTabBarMuh").setSelectedKey(routeParam);
			this.getData();
			this.handleIconTabBarSelect(e);

		},
		MuhasebeOnayBekApprove: function (e) {
			var that = this;
			var table = this.getView().byId("MuhasebeOnayBekTable");
			var selectedIndices = table.getSelectedIndices();
			// var selectedIndices = table.getBinding("rows").aIndices;
			// var model = this.getView().getModel("MuhOnayBekTalep");
			// var model = table.getBinding().oModel.getData().dataList;
			// var model = table.getContextByIndex(selectedIndices[0]).getObject().Odmfn;
			var temp = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				// temp.push(table.getRows()[i].getCells()[0].getText());
				// temp.push(model.oList[selectedIndices[i]].Odmfn);
				temp.push(table.getContextByIndex(selectedIndices[i]).getObject().Odmfn);
			}
			var data = {
				"Prcss": "Onay",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
				"MuhOnayTalep": [] //navigation property name
			};

			for (var x = 0; x < temp.length; x++) {
				var Selectedrow = {
					"Odmfn": temp[x]
				};
				data.MuhOnayTalep.push(Selectedrow);
			}

			that.createModel = new JSONModel(data);
			that.getView().setModel(that.createModel, "createCollection");
			var oSOData = this.getView().getModel("createCollection").getData();

			var oModel = this.getOwnerComponent().getModel();
			// var mParameters = {
			// 	success: that._handleCreateSuccess,
			// 	error: that._handleCreateError
			// };
			// oModel.create("/ProcessSet", oSOData, mParameters);
			oModel.create("/ProcessSet", oSOData, {
				method: "POST",
				header: "Content-Type: application/json",
				success: function (scc) {
					// var view = that.getView();
					// sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTaleplerData(e, view, "MuhasebeOnayBekTable",
					// 	"MuhOnayBekTalep",
					// 	"/dataList");
					MessageToast.show("Onaylama İşleminiz Başarı ile Gerçekleştirildi.");
					// table.setBusy(true);
					that.getView().byId("MuhasebeOnayBekTable").getBinding("rows").refresh();
					that.getView().byId("MuhasebeOnayBekTable").clearSelection();
					that.getData();
					// table.setBusy(false);
					// oModel.refresh();
				},
				error: function (err) {
					MessageToast.show("Kayıt İşlemi Sırasında Hata Oluştu.");
				}
			});
		},

		MuhasebeOnayBekReject: function (e) {
			var that = this;
			var table = this.getView().byId("MuhasebeOnayBekTable");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("MuhOnayBekTalep");
			var temp = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				temp.push(table.getContextByIndex(selectedIndices[i]).getObject().Odmfn);
			}
			var data = {
				"Prcss": "Red",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
				"MuhOnayTalep": []
			};

			for (var x = 0; x < temp.length; x++) {
				var Selectedrow = {
					"Odmfn": temp[x],
					"Redndn": model.getData().dataList[selectedIndices[x]].Redndn
				};
				data.MuhOnayTalep.push(Selectedrow);
			}
			var control = [];
			for (var y = 0; y < data.YonOnayTalep.length; y++) {
				control.push(data.MuhOnayTalep[y].Redndn);
			}
			if (control.includes("")) {
				MessageBox.warning("Lütfen Reddetme Nedenlerini Dolu giriniz.");
			} else {

				that.createModel = new JSONModel(data);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();

				var oModel = this.getOwnerComponent().getModel();
				// var mParameters = {
				// 	success: that._handleCreateSuccess,
				// 	error: that._handleCreateError
				// };
				// oModel.create("/ProcessSet", oSOData, mParameters);
				oModel.create("/ProcessSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						MessageToast.show("Reddetme İşleminiz Başarı ile Gerçekleşti.");
						that.getView().byId("MuhasebeOnayBekTable").getBinding("rows").refresh();
						that.getView().byId("MuhasebeOnayBekTable").clearSelection();
						that.getData();
						// oModel.refresh();
					},
					error: function (err) {
						MessageToast.show("Reddetme İşlemi Sırasında Bir Hata Oluştu.");
					}
				});
			}
		},

		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
						// 	window.onpopstate = function () {
						// 		window.history.pushState(null, "", window.location.href);
						// 	};
						}
					}
				}
			);
		},

		handleIconTabBarSelect: function (e) {
			var that = this;
			var tabBar = this.getView().byId("IconTabBarMuh");
			var tab = this.getView().byId("IconTabBarMuh").getSelectedKey();
			var table = this.getView().byId("MuhasebeOnayBekTable");
			var aFilter = new sap.ui.model.Filter({
				and: false,
				filters: []
			});

			if (tabBar) {
				switch (tab) {
				case "1":
					aFilter.aFilters.push(
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "06")
					);
					// filter.push(new Filter("Ostat", FilterOperator.EQ, "06"));
					that.getView().byId("btnAcceptMuh").setEnabled(true);
					that.getView().byId("btnRejectMuh").setEnabled(true);
					break;
				case "2":
					aFilter.aFilters.push(
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "07"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "08"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "09")
					);
					that.getView().byId("btnAcceptMuh").setEnabled(false);
					that.getView().byId("btnRejectMuh").setEnabled(false);
					break;
				case "3":
					aFilter.aFilters.push(
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "10")
					);
					that.getView().byId("btnAcceptMuh").setEnabled(false);
					that.getView().byId("btnRejectMuh").setEnabled(false);
					break;
				case "4":
					aFilter.aFilters.push(
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "06"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "07"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "08"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "09"),
						new sap.ui.model.Filter("Ostat", sap.ui.model.FilterOperator.EQ, "10")
					);
					// that.getView().byId("btnAcceptMuh").setEnabled(false);
					// that.getView().byId("btnRejectMuh").setEnabled(false);
					break;
				default:
					break;
				}
				table.getBinding("rows").filter([aFilter]);
				// table.setBusy(false);
				// sap.ui.core.BusyIndicator.hide();
				that.showTotalCost();
				table.setVisibleRowCountMode("Fixed");
				var x = window.innerHeight - 213;
				var count = Math.floor(x / 45);
				table.setVisibleRowCount(count - 3);
				// if (oBinding) {
				// 	oBinding.filter([]);
				// 	oBinding.filter(filter);
				// 	sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTaleplertalCost(view, oBinding);
				// }
			}
		},
		showTotalCost: function () {
			var that = this;
			var tab = this.getView().byId("IconTabBarMuh").getSelectedKey();
			var model = this.getView().getModel("MuhOnayBekTalep");
			var table = this.getView().byId("MuhasebeOnayBekTable");
			if (model) {
				var data = model.getProperty("/dataList");
				if (data.length > 0) {
					var filterData = data.filter(function (fil) {
						// return fil.Onyst === "0" + tab && fil.Ostat === "02" && tab !== 1 ? fil;
						// return fil.Onyst === "0" + tab && tab !== 1 ? fil : fil.Ostat === "02" && fil;
						// return fil.Onyst === "0" + tab && tab !== "1" ? fil : fil.Ostat === "02" && fil;
						if (tab === "1") {
							return fil.Ostat === "06";
						} else if (tab === "2") {
							return fil.Ostat === "07" || fil.Ostat === "08" || fil.Ostat === "09";
						} else if (tab === "3") {
							return fil.Ostat === "10";
						} else if (tab === "4") {
							return fil.Ostat === "06" || fil.Ostat === "07" || fil.Ostat === "08" || fil.Ostat === "09" || fil.Ostat === "10";
						}
					});
					table.setVisibleRowCountMode("Fixed");
					var x = window.innerHeight - 213;
					var count = Math.floor(x / 45);
					table.setVisibleRowCount(count - 3);
					// table.setVisibleRowCountMode("Fixed");
					// table.setVisibleRowCount(filterData.length + 1);
					table.setAlternateRowColors(true);
					var costs = [];
					for (var j = 0; j < filterData.length; j++) {
						costs.push({
							pb: filterData[j].Waers,
							cost: parseFloat(filterData[j].Odtut)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});
					// console.log(result);
				}
			}
			var costModel = new JSONModel(result);
			costModel.setProperty("/dataList", result);
			this.getView().setModel(costModel, "costData");
		},

		showDetailMuh: function (oEvent) {
			// var that = this;
			// var table = this.getView().byId("MuhasebeOnayBekTable");
			// // var Name1 = table.getContextByIndex(oEvent.getSource().oParent.getIndex()).getObject().Name1;
			// var Cari = table.getContextByIndex(oEvent.getSource().getParent().getIndex()).getObject().Cari;
			// var model = new JSONModel();
			// var Odmfn = oEvent.getSource().getText();
			// var view = this.getView();
			// model.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OdmfnDetaySet?$filter=Odmfn eq '" + Odmfn + "'");
			// model.attachRequestCompleted(function onComplete(e) {
			// 	if (e.getParameter("success")) {
			// 		sap.ui.core.BusyIndicator.hide();
			// 		var costs = [];
			// 		var data = e.getSource().getData().d.results;
			// 		var detailModel = new JSONModel();
			// 		detailModel.setProperty("/dataList", data);
			// 		if (data.length === 0) {
			// 			MessageBox.warning(Odmfn + " " + "numaralı formun detay bilgisi bulunmamaktadır!");
			// 		} else {
			// 			if (!that.oDialog2) {
			// 				that.oDialog2 = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.MuhasebeOdemeFormNoDetay", this);
			// 				that.getView().addDependent(that.oDialog2);
			// 			}
			// 			that.oDialog2.removeAllButtons();
			// 			that.oDialog2.addButton(new sap.m.Button({
			// 				text: "Kapat",
			// 				press: function () {
			// 					that.oDialog2.close();
			// 					that.oDialog2.destroy();
			// 					that.oDialog2 = null;
			// 				}
			// 			}));
			// 			that.oDialog2.open();
			// 			if (Cari === "") {
			// 				for (var i = 0; i < data.length; i++) {
			// 					detailModel.getData().dataList[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
			// 						detailModel.getData().dataList[i].Vadet);
			// 					costs.push({
			// 						pb: detailModel.getData().dataList[i].Waers,
			// 						cost: parseFloat(detailModel.getData().dataList[i].Odmtu)
			// 					});
			// 				}
			// 				that.getView().setModel(detailModel, "formnDetailData");
			// 				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTaleplertalCost(detailModel, costs, view);
			// 				sap.ui.getCore().byId("MuhdetailBukrs").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailBelnr").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailGjahr").setVisible(false);

			// 				sap.ui.getCore().byId("MuhdetailWrbtr").setVisible(false);
			// 			} else {
			// 				for (var j = 0; j < data.length; j++) {
			// 					detailModel.getData().dataList[j].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
			// 						detailModel.getData().dataList[j].Vadet);
			// 					costs.push({
			// 						pb: detailModel.getData().dataList[j].Waers,
			// 						cost: parseFloat(detailModel.getData().dataList[j].Wrbtr)
			// 					});
			// 				}
			// 				that.getView().setModel(detailModel, "formnDetailData");
			// 				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTaleplertalCost(detailModel, costs, view);
			// 				sap.ui.getCore().byId("MuhdetailLifnr").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailName1").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailOdmtu").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailWaers").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailBpbak").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailUpbak").setVisible(false);
			// 				sap.ui.getCore().byId("MuhdetailVdbak").setVisible(false);
			// 			}
			// 		}
			// 	}
			// });

			var table = this.getView().byId("MuhasebeOnayBekTable");
			var view = this.getView();
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").ShowDetailOdemeFormn(oEvent, table, view);
		},
		detailApprover: function (e) {
			var table = this.getView().byId("MuhasebeOnayBekTable");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").DetailApprover(e, Odmfn);
		},
		MuhdetailLİfnr: function (e) {
			MessageBox.show("aass");
		},
		deneme: function (e) {
			MessageBox.show("ddj");
		}
	});

});