sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox",
	"sap/m/Input",
	"sap/ui/model/json/JSONModel",
	"jquery.sap.global"
], function (Controller, MessageBox, Input, JSONModel, jQuery) {
	"use strict";

	var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
	var globalUserType, globalUserPernr, globalLoginData, globalUserNameSurname;

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.Login", {

		onInit: function () {
			var that = this;
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.onAfterRendering();
				}
			});
		},

		onAfterRendering: function () {
			this.getView().byId("inputUserName").setValue("");
			this.getView().byId("inputPassword").setValue("");
			globalLoginData = globalOStorage.get("myLocalData");
			if (globalLoginData) {
				var oModel = new JSONModel(globalLoginData);
				sap.ui.getCore().setModel(oModel, "globalLoginData");
				globalUserType = globalLoginData.localUserType;
				globalUserPernr = globalLoginData.localUserPernr;
				globalUserNameSurname = globalLoginData.localUserNameSurname;
			}
			window.history.pushState(null, "", window.location.href);
			window.history.pushState(null, "", window.location.href);
		},

		getGlobalUserType: function () {
			return globalUserType;
		},

		getGlobalUserPernr: function () {
			return globalUserPernr;
		},

		getGlobalUserNameSurname: function () {
			return globalUserNameSurname;
		},

		getGlobalOStorage: function () {
			return globalOStorage;
		},

		Go_To_Dashboard_Via_Login: function (oEvent) {
			var that = this;
			var userName = this.getView().byId("inputUserName").getValue();
			var password = this.getView().byId("inputPassword").getValue();

			var loginModel = new JSONModel();
			loginModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/LoginSet(Kulad='" + userName + "',Sifre='" + password + "')");
			loginModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					var loginData = oEvent.getSource().getData().d;
					if (userName === "" || password === "") {
						sap.m.MessageBox.alert("Kullanıcı adı veya şifre boş olamaz!", {
							title: "Uyarı"
						});
					} else if (loginData.Dogru) {
						var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
						if (loginData.Yntc && loginData.Mhsbc) {
							globalUserType = 3;
						} else if (loginData.Yntc) {
							globalUserType = 2;
						} else if (loginData.Mhsbc) {
							globalUserType = 1;
						} else {
							globalUserType = 0;
						}
						globalUserPernr = loginData.Pernr;
						globalUserNameSurname = loginData.Ename;
						globalLoginData = {
							"localUserType": globalUserType.toString(),
							"localUserPernr": globalUserPernr.toString(),
							"localUserNameSurname": globalUserNameSurname,
							"flagForSuggestedList": "0",
							"flagForOpenPaymentsList": "0"
						};
						globalOStorage.put("myLocalData", globalLoginData);
						var oModel = new JSONModel(globalLoginData);
						sap.ui.getCore().setModel(oModel, "globalLoginData");
						oRouter.navTo("Target_Dashboard", {});
						// window.location.reload();
					} else {
						sap.m.MessageBox.alert(loginData.Mesaj, {
							title: "Uyarı"
						});
					}
				}
			});
		}

	});

});