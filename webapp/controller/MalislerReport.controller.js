sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox"
], function (Controller, JSONModel, MessageBox) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.MalislerReport", {

		onInit: function () {
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
			var that = this;
			var nowDate = new Date();
			nowDate = nowDate.toISOString().split(".")[0];

			var oldDate = new Date();
			oldDate.setFullYear(oldDate.getFullYear() - 1);
			oldDate = oldDate.toISOString().split(".")[0];

			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.getParams(nowDate, oldDate);
				}
			});
			this.byId("DRS4").setDateValue(new Date());
			this.byId("DRS4").setSecondDateValue(new Date());
		},
		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function() {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		getParams: function (firstDate, secondDate) {
			var that = this;
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var routeParam = oRouter.getRoute("Target_Dashboard")._aRoutes[0]._router._prevRoutes[0].params[0];
			var MalislerModel = new JSONModel();
			if (routeParam === "f1") {
				MalislerModel.loadData(
					"/sap/opu/odata/sap/ZEO_GW_SRV/OdemeRaporuSet?$filter=Ostat eq '09' and ( Cpudt le datetime'" + firstDate +
					"' and Cpudt ge datetime'" + secondDate + "' )"
				);
			}
			// else if (routeParam === "f2") {
			// 	OdemeTaleplerimModel.loadData(
			// 		"/sap/opu/odata/sap/ZEO_GW_SRV/OdemeRaporuSet?$filter=Ostat eq '09'"
			// 	);
			// } else if (routeParam === "f3") {
			// 	OdemeTaleplerimModel.loadData(
			// 		"/sap/opu/odata/sap/ZEO_GW_SRV/OdemeRaporuSet?$filter=Ostat eq '09'"
			// 	);
			// } else {
			// 	OdemeTaleplerimModel.loadData(
			// 		"/sap/opu/odata/sap/ZEO_GW_SRV/OdemeRaporuSet?$filter=Ostat eq '09'"
			// 	);
			// }
			MalislerModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					for (var i = 0; i < data.length; i++) {
						data[i].Cpudt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Cpudt);
						data[i].Zfbdt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Zfbdt);
						data[i].Bldat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Bldat);
					}

					var table = that.getView().byId("malislerTable");
					table.setVisibleRowCountMode("Fixed");
					var x = window.innerHeight - 213;
					var count = Math.floor(x / 45);
					table.setVisibleRowCount(count - 3);

					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					that.getView().setModel(newModel, "Malisler");

					var costs = [];
					for (var j = 0; j < data.length; j++) {
						costs.push({
							pb: data[j].Waers,
							cost: parseFloat(data[j].Wrbtr)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});

					var costModel = new JSONModel(result);
					costModel.setProperty("/dataList", result);
					that.getView().setModel(costModel, "costData");
				}
			});
		},
		handleChange: function (e) {
			var dateFrom = e.getSource().getDateValue();
			var dateTo = e.getSource().getSecondDateValue();
			dateFrom.setHours(3, 0, 0, 0);
			dateTo.setHours(23, 59, 59, 59);
			dateFrom = dateFrom.toISOString().split(".")[0];
			dateTo = dateTo.toISOString().split(".")[0];
			this.getParams(dateTo, dateFrom);
		},
		detailApprover: function (e) {
			var table = this.getView().byId("malislerTable");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").DetailApprover(e, Odmfn);
		},
		detailOdmfn: function (e) {
			var table = this.getView().byId("malislerTable");
			var view = this.getView();
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").ShowDetailOdemeFormn(e, table, view);
		}
	});

});