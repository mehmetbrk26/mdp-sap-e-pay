sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/m/MessageBox"
], function (Controller, MessageBox) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OdemeTaleplerim", {

		onInit: function () {
			
		},
		
		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard", {});
		},
		
		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function() {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		}
		
	});

});