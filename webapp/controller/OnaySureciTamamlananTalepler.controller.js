sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/m/MessageBox",
	"sap/m/MessageToast",
], function (Controller, JSONModel, MessageBox, MessageToast) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OnaySureciTamamlananTalepler", {

		onInit: function () {
			sap.ui.core.BusyIndicator.show();
			var that = this;
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());

			var nowDate = new Date();
			nowDate = nowDate.toISOString().split(".")[0];

			var oldDate = new Date();
			oldDate.setFullYear(oldDate.getFullYear() - 1);
			oldDate = oldDate.toISOString().split(".")[0];

			this.getData(nowDate, oldDate);

			this.byId("DRS3").setDateValue(new Date());
			this.byId("DRS3").setSecondDateValue(new Date());
		},

		getData: function (firstDate, secondDate) {
			var that = this;
			var OnaySurecTamam = new JSONModel();
			OnaySurecTamam.loadData(
				"/sap/opu/odata/sap/ZEO_GW_SRV/OnaySurecTamamSet?$filter=Ostat eq '07' and ( Cpudt le datetime'" + firstDate +
				"' and Cpudt ge datetime'" + secondDate + "' )"
				// "/sap/opu/odata/sap/ZEO_GW_SRV/OnaySurecTamamSet?$filter=Ostat eq '07' and ( Cpudt le datetime'2020-06-05T00:00:00' and Cpudt ge datetime'2019-06-05T00:00:00' )"
			);

			OnaySurecTamam.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					for (var i = 0; i < data.length; i++) {
						data[i].Cpudt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Cpudt);
						data[i].Zfbdt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Zfbdt);
						data[i].Bldat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Bldat);
					}

					// var table = this.getView().byId("OnaySurecTamam");
					// table.setVisibleRowCountMode("Fixed");
					// var x = window.innerHeight - 213;
					// var count = Math.floor(x / 45);
					// table.setVisibleRowCount(count);

					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					that.getView().setModel(newModel, "onaySureciTamam");

					var costs = [];
					for (var j = 0; j < data.length; j++) {
						costs.push({
							pb: data[j].Waers,
							cost: parseFloat(data[j].Wrbtr)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});

					var costModel = new JSONModel(result);
					costModel.setProperty("/dataList", result);
					that.getView().setModel(costModel, "costData");
				}
			});
		},

		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi
								._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function() {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		sendMail: function (e) {
			var that = this;
			var table = this.getView().byId("OnaySurecTamam");
			var model = this.getView().getModel("onaySureciTamam");
			var modelData = model.getData().dataList;
			var selectedIndices = table.getSelectedIndices();
			if (selectedIndices.length === 0) {
				MessageBox.show("Mail göndermek istediğiniz alanların işaretli olduğundan emin olunuz.");
			} else {
				var temp = [];
				for (var i = 0; i < selectedIndices.length; i++) {
					temp.push(modelData[selectedIndices[i]]);
				}
				var data = {
					"Prcss": "Mail",
					"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr().toString(),
					"IslOnayTam": []
				};

				for (var x = 0; x < temp.length; x++) {
					var Selectedrow = {
						"Odmfn": temp[x].Odmfn,
						"Bukrs": temp[x].Bukrs,
						"Lifnr": temp[x].Lifnr,
						"Kunnr": temp[x].Kunnr,
						"Name1": temp[x].Name1,
						"Waers": temp[x].Waers,
						"Wrbtr": temp[x].Wrbtr,
						"Gsber": temp[x].Gsber,
						"Pernr": temp[x].Pernr,
						"Ename": temp[x].Ename,
						"Cpudt": temp[x].Cpudt,
						"Bldat": temp[x].Bldat,
						"Zfbdt": temp[x].Zfbdt,
						"Odmtp": temp[x].Odmtp,
						"Odmta": temp[x].Odmta,
						"Banka": temp[x].Banka,
						"Bankn": temp[x].Bankn,
						"Iban": temp[x].Iban,
						"Bvtyp": temp[x].Bvtyp,
						"Ostat": temp[x].Ostat,
						"OstatDesc": temp[x].OstatDesc,
						"Xezer": temp[x].Xezer
					};
					data.IslOnayTam.push(Selectedrow);
				}
				that.createModel = new JSONModel(data);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();

				var oModel = this.getOwnerComponent().getModel();

				oModel.create("/ProcessSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						MessageToast.show("Mail Gönderme İşleminiz Başarı ile Gerçekleştirildi.");
						// oModel.refresh();
					},
					error: function (err) {
						MessageToast.show("Mail Gönderme İşlemi Sırasında Bir Hata Oluştu.");
					}
				});
			}
		},
		handleChange: function (e) {
			var dateFrom = e.getSource().getDateValue();
			var dateTo = e.getSource().getSecondDateValue();
			dateFrom.setHours(3, 0, 0, 0);
			dateTo.setHours(23, 59, 59, 59);
			dateFrom = dateFrom.toISOString().split(".")[0];
			dateTo = dateTo.toISOString().split(".")[0];
			this.getData(dateTo, dateFrom);
		},
		openDetail: function (e) {
			var that = this;
			var table = this.getView().byId("OnaySurecTamam");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			var Bukrs = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Bukrs;
			var Waers = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Waers;
			var Cari = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Cari;
			var Koart = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Koart;
			var detailModel = new JSONModel();

			detailModel.loadData(
				"/sap/opu/odata/sap/ZEO_GW_SRV/CariKalemSet?$filter=Bukrs eq '" + Bukrs + "' and Cari eq '" + Cari + "' and Waers eq '" +
				Waers + "' and Koart eq '" + Koart + "' and Odmfn eq '" + Odmfn + "'"
			);
			if (!that.oDialog2) {
				that.oDialog2 = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.Detail", this);
				that.getView().addDependent(that.oDialog2);
			}
			that.oDialog2.removeAllButtons();
			that.oDialog2.addButton(new sap.m.Button({
				text: "Kapat",
				press: function () {
					that.oDialog2.close();
					that.oDialog2.destroy();
					that.oDialog2 = null;
				}
			}));
			that.oDialog2.open();
			detailModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					// for (var i = 0; i < data.length; i++) {
					// 	data[i].Cpudt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(data[i].Cpudt);
					// 	data[i].Zfbdt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(data[i].Zfbdt);
					// 	data[i].Bldat = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(data[i].Bldat);
					// }
					var newModel = new JSONModel();
					newModel.setProperty("/detailList", data);
					that.getView().setModel(newModel, "detailModel");
				}
			});
		},
		detailApprover: function (e) {
			var table = this.getView().byId("OnaySurecTamam");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").DetailApprover(e, Odmfn);
		},
		showDetailForm: function (e) {
			var table = this.getView().byId("OnaySurecTamam");
			var view = this.getView();
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").ShowDetailOdemeFormn(e, table, view);
		}

	});
});