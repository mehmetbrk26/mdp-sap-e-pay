sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/viz/ui5/controls/common/feeds/AnalysisObject",
	"sap/suite/ui/commons/ChartContainerContent",
	"sap/viz/ui5/controls/VizFrame",
	"sap/ui/model/json/JSONModel",
	"sap/viz/ui5/data/FlattenedDataset",
	"sap/viz/ui5/controls/common/feeds/FeedItem",
	"sap/m/Label",
	"sap/m/MessageBox",
	"sap/ui/integration/library"
], function (Controller, AnalysisObject, ChartContainerContent, VizFrame, JSONModel, FlattenedDataset, FeedItem, Label, MessageBox,
	integrationLibrary) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.Dashboard", {

		onInit: function () {
			var that = this;
			var currentDate = new Date();
			currentDate.setHours(3, 0, 0, 0);
			currentDate = currentDate.toISOString().split(".")[0];
			var date = new Date();

			var firstDate = new Date(date.getFullYear(), date.getMonth(), 1);
			firstDate.setHours(23, 59, 59, 59);
			firstDate = firstDate.toISOString().split(".")[0];

			this.byId("DP10").setDateValue(new Date());
			this.byId("DP10").setMaxDate(new Date());
			this.byId("DP10").setMinDate(new Date(2019, 12, 1));
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.onAfterRendering();
					that.stackedBar(firstDate, currentDate);
					that.donutChart(firstDate, currentDate);
				}
			});

		},

		onAfterRendering: function () {
			var oCardManifests = new JSONModel("./cardManifests.json");
			this.getView().setModel(oCardManifests, "manifests");
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
			this.initializeEventListener(this);
		},

		initializeEventListener: function (that) {
			//	const aAreas = document.querySelectorAll('area.areaMap');
			//	aAreas.forEach(elem => elem.addEventListener('click', function(oEvent) { //Attach click event listener to areas
			//		that.takeAction(that, oEvent.target.dataset.areaselected);
			//	}));
		},
		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi
								._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},
		takeAction: function (that, selectedArea) {
			//Your code 
		},
		handleChange: function (e) {
			var currentDate = new Date();
			var month = e.getSource().getDateValue().getMonth() + 1;
			var firstDate = new Date(currentDate.getFullYear(), currentDate.getMonth());
			// var lastDate = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 0);
			var selectedDate = e.getSource().getDateValue();
			var fselectedDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), 1);
			var lselectedDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth() + 1, 0);
			if (month < currentDate.getMonth() + 1) {
				this.byId("DRS5").setDateValue(fselectedDate);
				this.byId("DRS5").setSecondDateValue(lselectedDate);
			} else {
				this.byId("DRS5").setDateValue(firstDate);
				this.byId("DRS5").setSecondDateValue(currentDate);
			}
			fselectedDate.setHours(3, 0, 0, 0);
			lselectedDate.setHours(23, 59, 59, 59);
			fselectedDate = fselectedDate.toISOString().split(".")[0];
			lselectedDate = lselectedDate.toISOString().split(".")[0];

			this.donutChart(fselectedDate, lselectedDate);
			this.stackedBar(fselectedDate, lselectedDate);

		},
		donutChart: function (firstDate, secondDate) {
			var that = this;
			var vizFrame = this.getView().byId("chart");
			vizFrame.removeAllFeeds();
			var oModel = new JSONModel();
			var dashboardGraphicModel = new JSONModel();
			// var oTextView = new sap.m.Text({
			// 	text: "Data Not Found"
			// });
			dashboardGraphicModel.loadData(
				"/sap/opu/odata/sap/ZEO_GW_SRV/DashboardGraphicASet?$filter=Odmtar ge datetime'" + firstDate +
				"' and Odmtar le datetime'" + secondDate + "'"
			);
			dashboardGraphicModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					if (data.length === 0) {
						var date = new Date();
						var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
						var lastDay = new Date(date.getFullYear(), date.getMonth() - 1 + 1, 0);
						// that.byId("DRS5").setDateValue(firstDay);
						firstDay.setHours(23, 59, 59, 59);
						firstDay = firstDay.toISOString().split(".")[0];
						lastDay = lastDay.toISOString().split(".")[0];
						dashboardGraphicModel.loadData(
							"/sap/opu/odata/sap/ZEO_GW_SRV/DashboardGraphicASet?$filter=Odmtar ge datetime'" + firstDay +
							"' and Odmtar le datetime'" + lastDay + "'"
						);
					} else {
						for (var i = 0; i < data.length; i++) {
							data[i].Odmtar = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Odmtar);
						}
					}

					var reduceDataOdmta =
						data.reduce(function (r, a) {
							r[a.Odmta] = r[a.Odmta] || [];
							r[a.Odmta].push(a);
							return r;
						}, {});

					var OdmtaArr = Object.entries(reduceDataOdmta);

					var endData = [];
					for (var j = 0; j < OdmtaArr.length; j++) {
						endData.push({
							"name": OdmtaArr[j][0],
							"number": OdmtaArr[j][1].length
						});
					}
					var newModel = new JSONModel();
					newModel.setProperty("/dataList", endData);
					that.getView().setModel(newModel, "dashboardDonut");
					oModel.setData(endData);
				}
			});

			var dataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: "Ödeme Türü",
					value: "{name}"
				}],
				measures: [{
					name: "Toplam",
					value: "{number}"
				}],
				data: {
					path: "dashboardDonut>/dataList"
				}
			});
			vizFrame.setDataset(dataset);
			vizFrame.setModel(oModel);

			vizFrame.setVizProperties({
				title: {
					text: "Ödeme Türüne Göre Detaylar"
				},
				plotArea: {
					// colorPalette: d3.scale.category20().range(),
					colorPalette: ["#FF671F", "#777777", "#2AAFDC", "#999999", "#F8C659", "#F8E7A8"]
						// drawwingEffect: "glossy"
				},
				legend: {
					title: {
						visible: false
					}
				},
				legendGroup: {
					layout: {
						position: "bottom"
					}
				}
			});
			var feedsize = new sap
				.viz.ui5.controls.common.feeds.FeedItem({
					uid: "size",
					type: "Measure",
					values: ["Toplam"]
				}),
				feedcolor = new sap.viz.ui5.controls.common.feeds.FeedItem({
					uid: "color",
					type: "Dimension",
					values: ["Ödeme Türü"]
				});
			vizFrame.addFeed(feedsize);
			vizFrame.addFeed(feedcolor);
		},
		stackedBar: function (firstDate, secondDate) {
			var that = this;
			var oVizFrame = this.getView().byId("idcolumn");
			oVizFrame.removeAllFeeds();
			var oModel = new JSONModel();
			var dashboardGraphicModel = new JSONModel();
			dashboardGraphicModel.loadData(
				"/sap/opu/odata/sap/ZEO_GW_SRV/DashboardGraphicASet?$filter=Odmtar ge datetime'" + firstDate +
				"' and Odmtar le datetime'" + secondDate + "'"
			);
			dashboardGraphicModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;

					if (data.length === 0) {
						var date = new Date();
						var firstDay = new Date(date.getFullYear(), date.getMonth() - 1, 1);
						var lastDay = new Date(date.getFullYear(), date.getMonth() - 1 + 1, 0);

						firstDay.setHours(23, 59, 59, 59);
						firstDay = firstDay.toISOString().split(".")[0];
						lastDay = lastDay.toISOString().split(".")[0];
						dashboardGraphicModel.loadData(
							"/sap/opu/odata/sap/ZEO_GW_SRV/DashboardGraphicASet?$filter=Odmtar ge datetime'" + firstDay +
							"' and Odmtar le datetime'" + lastDay + "'"
						);
					} else {
						for (var i = 0; i < data.length; i++) {
							data[i].Odmtar = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Odmtar);
						}
					}

					var reduceData =
						data.reduce(function (r, a) {
							r[a.Odmtar] = r[a.Odmtar] || [];
							r[a.Odmtar].push(a);
							return r;
						}, {});
					var testArr = Object.entries(reduceData);

					var ddd = [];
					var endData = [];
					for (var j = 0; j < testArr.length; j++) {
						var a = testArr[j][1];
						// console.log(a);
						for (var v = 0; v < a.length; v++) {
							ddd.push({
								pb: a[v].Waers,
								cost: parseFloat(a[v].Wrbtr)
							});
						}
						var result = [];
						ddd.reduce(function (res, value) {
							if (!res[value.pb]) {
								res[value.pb] = {
									pb: value.pb,
									cost: 0
								};
								result.push(res[value.pb]);
							}
							res[value.pb].cost += value.cost;
							return res;
						}, {});
						ddd = [];
						endData.push({
							tarih: testArr[j][1][0].Odmtar,
							total: result
						});
					}

					// var result = [];
					// cost.reduce(function (res, value) {
					// 	if (!res[value.pb]) {
					// 		res[value.pb] = {
					// 			pb: value.pb,
					// 			cost: 0
					// 		};
					// 		result.push(res[value.pb]);
					// 	}
					// 	res[value.pb].cost += value.cost;
					// 	return res;
					// }, {});

					// {
					//   tarih: '22.06.2020',
					//   data: [
					//     tl: 2200,
					//     usd: 500,
					//     eur: 1400,
					//	]
					// }
					var compData = [];
					for (var x = 0; x < testArr.length; x++) {
						var liraToplam = 0;
						var usdToplam = 0;
						var eurToplam = 0;
						testArr[x][1].filter(function (item) {
							if (item.Waers === "TRY") {
								liraToplam += parseInt(item.Wrbtr, 10);
							} else if (item.Waers === "USD") {
								usdToplam += parseInt(item.Wrbtr, 10);
							} else if (item.Waers === "EUR") {
								eurToplam += parseInt(item.Wrbtr, 10);
							}
						});
						compData.push({
							tarih: testArr[x][1][0].Odmtar,
							// lira: liraToplam,
							// usd: usdToplam,
							// eur: eurToplam
							data: []
						});
						compData[x].data.push({
							lira: liraToplam,
							usd: usdToplam,
							eur: eurToplam
						});
						// console.log(compData);
						// testArr[x].filter(function (fil) {
						// 	console.log(fil);

						// var result = [];
						// fil.reduce(function (res, value) {
						// 	if (!res[value.Waers]) {
						// 		res[value.Waers] = {
						// 			pb: value.Waers,
						// 			cost: 0
						// 		};
						// 		result.push(res[value.pb]);
						// 	}
						// 	res[value.pb].Wrbtr += value.Wrbtr;
						// 	return res;
						// }, {});
						// });
					}

					// var result = [];
					// data.reduce(function (res, value) {
					// 	if (!res[value.Odmtar]) {
					// 		res[value.Odmtar] = {
					// 			Odmtar: value.Odmtar,
					// 			Wrbtr: value.Wrbtr,
					// 			Waers: value.Waers
					// 		};
					// 		result.push(res[value.Odmtar]);
					// 	}
					// 	result.reduce(function (a, b) {

					// 	});
					// 	// res[value.Odmtar].Wrbtr += value.Wrbtr;
					// 	return res;
					// }, {});

					// var test = [];
					// var reduceData =
					// 	data.reduce(function (r, a) {
					// 		r[a.Odmtar] = r[a.Odmtar] || [];
					// 		r[a.Odmtar].push(a);
					// 		return r;
					// 	}, Object.create(null));
					// test.push(reduceData);
					// var result = [];
					// reduceData.reduce(function (res, value) {
					// 	if (!res[value.pb]) {
					// 		res[value.pb] = {
					// 			pb: value.pb,
					// 			cost: 0
					// 		};
					// 		result.push(res[value.pb]);
					// 	}
					// 	res[value.pb].cost += value.cost;
					// 	return res;
					// }, {});
					// var costModel = new JSONModel(result);
					// costModel.setProperty("/dataList", result);
					// this.getView.setModel(costModel, "costDataDetail");

					var newModel = new JSONModel();
					newModel.setProperty("/dataList", compData);
					that.getView().setModel(newModel, "dashboardGraphic");
					oModel.setData(compData);
				}
			});

			var oDataset = new sap.viz.ui5.data.FlattenedDataset({
				dimensions: [{
					name: "Gün",
					value: {
						parts: ["tarih"],
						formatter: function (oCreatestamp) {
							// if (oCreatestamp === null) {
							// 	return oCreatestamp;
							// }
							var oDateFormat = sap.ui.core.format.DateFormat.getDateTimeInstance({
								pattern: "dd.MM.yyyy"
							});
							return oDateFormat.format(oCreatestamp);
						}
					}
					// value: "{dashboardGraphic>Odmtar}",
					// dataType: "date"
				}],
				measures: [{
					name: "Türk Lirası",
					value: "{data/0/lira}"
						// value: "{total/0/cost}"
						// value: "{lira}"
				}, {
					name: "Dolar",
					value: "{data/0/usd}"
						// value: "{total/1/cost}"
				}, {
					name: "Euro",
					// value: "{total/2/cost}"
					value: "{data/0/eur}"
				}],
				// , {
				// 	name: "Pb",
				// 	// value: "{compData[0]/data[0]/lira}"
				// 	value: "TRY"
				// }
				data: {
					path: "dashboardGraphic>/dataList"
				}
			});
			oVizFrame.setDataset(oDataset);
			oVizFrame.setModel(oModel);
			// oVizFrame.setNoDataText(oTextView);
			oVizFrame.setVizType("column");

			oVizFrame.setVizProperties({
				plotArea: {
					// colorPalette: ["#FFBB78", "#ffda79", "#AEC7E8"],
					colorPalette: ["#2AAFDC", "#FF671F", "#999999"],
					dataLabel: {
						showTotal: true,
						visible: true
					}
				},
				legend: {
					title: {
						visible: false
					}
				},
				legendGroup: {
					layout: {
						position: "bottom"
					}
				},
				// plotArea: {
				// 	// colorPalette: "#f19066"
				// 	colorPalette: "#515151"
				// },
				title: {
					text: "Aylık Ödeme Detayları"
				}
			});

			var feedValueAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					"uid": "valueAxis",
					"type": "Measure",
					"values": ["Türk Lirası"]
				}),
				feedValueAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
					"uid": "valueAxis",
					"type": "Measure",
					"values": ["Dolar"]
				}),
				feedValueAxis2 = new sap.viz.ui5.controls.common.feeds.FeedItem({
					"uid": "valueAxis",
					"type": "Measure",
					"values": ["Euro"]
				}),
				feedCategoryAxis = new sap.viz.ui5.controls.common.feeds.FeedItem({
					"uid": "categoryAxis",
					"type": "Dimension",
					"values": ["Gün"]
				});
			// ,
			// feedCategoryAxis1 = new sap.viz.ui5.controls.common.feeds.FeedItem({
			// 	"uid": "categoryAxis",
			// 	"type": "Dimension",
			// 	"values": ["Pb"]
			// });
			oVizFrame.addFeed(feedValueAxis1);
			oVizFrame.addFeed(feedValueAxis2);
			oVizFrame.addFeed(feedValueAxis);
			oVizFrame.addFeed(feedCategoryAxis);
			// oVizFrame.addFeed(feedCategoryAxis1);

		},
		goDetailPayment: function (e) {
			var id = e.getParameters().id;
			id = id.split("--")[1];
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("OdemeTaleplerimReport", {
				router: id
			});
		},
		goDetailFinancial: function (e) {
			var id = e.getParameters().id;
			id = id.split("--")[1];
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("MalislerReport", {
				router: id
			});
		},

		action: function (oEvent) {
			var that = this;
			var actionParameters = JSON.parse(oEvent.getSource().data("wiring").replace(/'/g, "\""));
			var eventType = oEvent.getId();
			var aTargets = actionParameters[eventType].targets || [];
			aTargets.forEach(function (oTarget) {
				var oControl = that.byId(oTarget.id);
				if (oControl) {
					var oParams = {};
					for (var prop in oTarget.parameters) {
						oParams[prop] = oEvent.getParameter(oTarget.parameters[prop]);
					}
					oControl[oTarget.action](oParams);
				}
			});
			var oNavigation = actionParameters[eventType].navigation;
			if (oNavigation) {
				var oParams = {};
				(oNavigation.keys || []).forEach(function (prop) {
					oParams[prop.name] = encodeURIComponent(JSON.stringify({
						value: oEvent.getSource().getBindingContext(oNavigation.model).getProperty(prop.name),
						type: prop.type
					}));
				});
				if (Object.getOwnPropertyNames(oParams).length !== 0) {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName, oParams);
				} else {
					this.getOwnerComponent().getRouter().navTo(oNavigation.routeName);
				}
			}
		},

		goToTargetPaymentRequestEntry: function (oEvent) {
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "0" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "1" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "2" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "3") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("Target_Odeme_Talebi_Girisi", {});
			} else {
				MessageBox.alert("Ödeme talebi girişi için yetkiniz yok!");
			}
		},

		goToTargetWaitingPaymentRequestsOnMyApprovement: function (e) {
			// var locId = e.getSource().getFailedText();
			var locId = "1";
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "2" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "3") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("Target_Onayimda_Bekleyen_Talepler", {
					router: locId
				});
			} else {
				MessageBox.alert("Yetkiniz olmadığından onay bekleyen talepleri göremezsiniz!");
			}
		},

		goToTargetMyPaymentRequests: function (oEvent) {
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "0" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "1" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "2" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "3") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("Target_Odeme_Taleplerim", {});
			} else {
				MessageBox.alert("Yetkiniz olmadığından ödeme taleplerinizi göremezsiniz!");
			}
		},

		goToTargetWaitingPaymentRequestsOnAcoountancy: function (e) {
			// var locId = e.getSource().getFailedText();
			var locId = "1";
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "1" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "3") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("Target_Muhasebe_Onayi_Bekleyen_Talepler", {
					router: locId
				});
			} else {
				MessageBox.alert("Yetkiniz olmadığından muhasebe onayı bekleyen talepleri göremezsiniz!");
			}
		},

		goToTargetCompletedPaymentRequests: function (oEvent) {
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "0" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "1" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "2" ||
				sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserType().toString() === "3") {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				oRouter.navTo("Target_Onay_Sureci_Tamamlanan_Talepler", {});
			} else {
				MessageBox.alert("Yetkiniz olmadığından tamamlanmış ödeme taleplerinizi göremezsiniz!");
			}
		},

		unixToDate: function (val) {
			if (val !== undefined && val !== "" && val !== null) {
				var unixDate = val.substr(6, 13);
				var date = new Date(parseInt(unixDate, 10));
				var dateFormat = sap.ui.core.format.DateFormat.getDateInstance({
					pattern: "dd.MM.yyyy"
				});
				var value = dateFormat.format(date);
				return value;
			}
		},
		unixToDate1: function (val) {
			if (val !== undefined && val !== "" && val !== null) {
				var unixDate = val.substr(6, 13);
				var date = new Date(parseInt(unixDate, 10));
				return date;
			}
		},

		Time: function (val) {
			if (val) {
				val = val.replace(/^PT/, "").replace(/S$/, "");
				val = val.replace("H", ":").replace("M", ":");
				var multipler = 60 * 60;
				var result = 0;
				val.split(":").forEach(function (token) {
					result += token * multipler;
					multipler = multipler / 60;
				});
				var timeinmiliseconds = result * 1000;
				var timeFormat = sap.ui.core.format.DateFormat.getTimeInstance({
					pattern: "HH:mm:ss"
				});
				var TZOffsetMs = new Date(0).getTimezoneOffset() * 60 * 1000;
				return timeFormat.format(new Date(timeinmiliseconds + TZOffsetMs));
			}
			return null;
		}
	});

});