sap.ui.define([
	'jquery.sap.global',
	'sap/ui/core/Fragment',
	'sap/ui/core/mvc/Controller',
	'sap/ui/model/json/JSONModel',
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator",
	"sap/ui/model/FilterType",
	"sap/ui/core/util/Export",
	"sap/ui/core/util/ExportTypeCSV",
	'sap/m/Token'
], function (jQuery, Fragment, Controller, JSONModel, MessageBox, Filter, FilterOperator, FilterType, Export, ExportTypeCSV, Token) {
	"use strict";

	var globalSelectedIndices = [],
		globalSelectedNYTs = [],
		globalInitFlag = 0;
	var globalVariantItemsList = [],
		globalDetailSuggestedItemSource = undefined;
	var inputId, valueHelpDialog, globalAccountCodeTitles = [],
		globalAccountCodeKeys = [], globalCounter = 0;
	var globalSelectedCompanyCode = 0,
		globalOneriListesiDetayData = [],
		globalQuickView = undefined;

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiOneriListesiDetayi", {
		
		onInit: function () {
			var that = this;
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.onAfterRendering();
				}
			});
		},

		onAfterRendering: function () {
			var that = this;
			that.getView().byId("allParameters").setExpanded(true);
			that.getView().byId("oneriListesi").setVisibleRowCount(6);
			that.getView().byId("oneriListesi").setVisibleRowCount(6);
			that.getView().byId("oneriListesi").setVisibleRowCount(6);
			that.getView().byId("SecondRegister").setVisible(false);
			// sap.ui.core.BusyIndicator.show();
			that.getView().byId("oneriListesi").setBusy(true);
			if (sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().get("myLocalData").flagForSuggestedList === "1" && globalCounter !== 2) {
				var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
				oRouter.navTo("Target_Odeme_Talebi_Girisi");
			} else {
				globalSelectedCompanyCode = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedCompanyCode();
				that.getView().byId("CompanyCode").setSelectedKey(globalSelectedCompanyCode);
				this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
				this.byId("OpenPaymentDate").setDateValue(new Date());
				this.byId("ReferenceDate").setDateValue(new Date());

				var companyCodeModel = new JSONModel();
				companyCodeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShSirketKodSet");
				companyCodeModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var companyCodeData = oEvent.getSource().getData().d.results;
						var cModel = new JSONModel(companyCodeData);
						that.getView().setModel(cModel, "companyCodeList");
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				var nytModel = new JSONModel();
				nytModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShNytSet");
				nytModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var nytData = oEvent.getSource().getData().d.results;
						var cModel = new JSONModel(nytData);
						that.getView().setModel(cModel, "nytList");
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				var currencyModel = new JSONModel();
				currencyModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShPBSet");
				currencyModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var currencyData = oEvent.getSource().getData().d.results;
						var cModel = new JSONModel(currencyData);
						cModel.setSizeLimit(300);
						that.getView().setModel(cModel, "currencyList");
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				var payCondModel = new JSONModel();
				payCondModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShPayCondSet");
				payCondModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var payCondData = oEvent.getSource().getData().d.results;
						var cModel = new JSONModel(payCondData);
						that.getView().setModel(cModel, "payCondList");
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				var payKindModel = new JSONModel();
				payKindModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShOdemeBicimiSet");
				payKindModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var payKindData = oEvent.getSource().getData().d.results;
						var cModel = new JSONModel(payKindData);
						that.getView().setModel(cModel, "payKindList");
						// sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				globalVariantItemsList = [];
				var variantModel = new JSONModel();
				variantModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/VariantHeaderSet?$filter=Pname eq 'ONERILIST'");
				variantModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var variantData = oEvent.getSource().getData().d.results;
						for (var i = 0; i < variantData.length; i++) {
							var newEntry = Object.create(null);
							newEntry.VAR_KEY = variantData[i].Pname.toString() + variantData[i].Variant.toString();
							newEntry.VAR_NAME = variantData[i].Variant.toString();
							newEntry.VAR_ENAME = variantData[i].Ename.toString();
							globalVariantItemsList.push(newEntry);
						}
						var cModel = new JSONModel(globalVariantItemsList);
						that.getView().setModel(cModel, "variantItemsList");
						sap.ui.core.BusyIndicator.hide();
						that.getView().byId("oneriListesi").setBusy(false);
					}
				});

				if (globalInitFlag === 1) {
					var x = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().get("myLocalData");
					x.flagForSuggestedList = "1";
					sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalOStorage().put("myLocalData", x);
				}
				globalInitFlag = 1;
				globalCounter++;
			}
		},

		clearAllFields: function (oView) {
			var that = this.getView();
			try {
				if (oView && oView.getViewName()) {
					that = oView;
				}
			} catch (err) {
				that = this.getView();
			}
			that.byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
			that.byId("OpenPaymentDate").setValueState(sap.ui.core.ValueState.None);
			that.byId("ReferenceDate").setValueState(sap.ui.core.ValueState.None);
			that.byId("Currency").setValueState(sap.ui.core.ValueState.None);
			that.byId("NYT").setValueState(sap.ui.core.ValueState.None);
			that.byId("accountCode").setValueState(sap.ui.core.ValueState.None);
			that.byId("CompanyCode").setValue("");
			that.byId("CompanyCode").setSelectedKey("");
			that.byId("OpenPaymentDate").setValue("");
			that.byId("ReferenceDate").setValue("");
			globalSelectedNYTs = [];
			that.byId("NYT").setEditable(true);
			that.byId("NYT").setSelectedItems([]);
			that.byId("NYT").setSelectedKeys([]);
			that.byId("checkBoxNYT").setSelected(true);
			globalAccountCodeKeys = [];
			globalAccountCodeTitles = [];
			that.byId("accountCode").removeAllTokens();
			// globalAccountCodeKey = "";
			that.byId("accountCode").setValue("");
			that.byId("accountCode").setEditable(false);
			that.byId("checkBoxAC").setSelected(false);
			that.byId("PaymentCondition").setValue("");
			that.byId("PaymentCondition").setSelectedKey("");
			that.byId("PaymentKind").setValue("");
			that.byId("PaymentKind").setSelectedKey("");
			that.byId("ReferenceHeaderKey").setValue("");
			that.byId("Currency").setValue("");
			that.byId("Currency").setSelectedKey("");
			that.byId("checkBoxWithoutRHK").setSelected(false);
			that.byId("checkBoxWithRHK").setSelected(false);
			that.byId("checkBoxSperr").setSelected(true);
			that.byId("checkBoxZahls").setSelected(true);
			that.byId("checkBoxBorcBak").setSelected(true);
			that.byId("vm").clearVariantSelection();
			
			globalSelectedIndices = [];
			that.byId("oneriListesi").clearSelection();
			
			globalOneriListesiDetayData = [];
			var cModel = new JSONModel(globalOneriListesiDetayData);
			that.setModel(cModel, "oneriListesiDetayList");
			var totalList = {
				"cost": "0,00",
				"pb": " "
			};
			var costModel = new JSONModel(totalList);
			costModel.setProperty("/dataList", totalList);
			that.setModel(costModel, "costData");
			
			globalSelectedCompanyCode = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").getGlobalSelectedCompanyCode();
			that.byId("CompanyCode").setSelectedKey(globalSelectedCompanyCode);
			that.byId("OpenPaymentDate").setDateValue(new Date());
			that.byId("ReferenceDate").setDateValue(new Date());
		},

		getRealtedDatasViaCompanyCode: function (oEvent) {
			var that = this;
			globalSelectedCompanyCode = that.getView().byId("CompanyCode").getSelectedKey();
		},

		getSelectedNYTs: function (oEvent) {
			globalSelectedNYTs = oEvent.getParameter("selectedItems");
		},

		getTableDatas: function (digit) {
			var that = this;
			// sap.ui.core.BusyIndicator.show();
			that.getView().byId("oneriListesi").setBusy(true);
			var localCompany = that.getView().byId("CompanyCode").getSelectedKey();
			var localKeyDate = that.getView().byId("OpenPaymentDate").getValue();
			var localRvadt = that.getView().byId("ReferenceDate").getValue();
			var localPB = that.getView().byId("Currency").getSelectedKey();
			this.getView().byId("allParameters").setExpanded(false);
			globalOneriListesiDetayData = [];

			if (localCompany && localKeyDate && localRvadt && localPB) {
				that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.None);
				that.getView().byId("OpenPaymentDate").setValueState(sap.ui.core.ValueState.None);
				that.getView().byId("ReferenceDate").setValueState(sap.ui.core.ValueState.None);
				that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.None);
				if (localKeyDate) {
					if (localKeyDate.substring(0, 2).includes(".")) {
						localKeyDate = localKeyDate.substring(5, 9) + "-" + localKeyDate.substring(2, 4) + "-0" + localKeyDate.substring(0, 1) +
							"T00:00:00";
					} else {
						localKeyDate = localKeyDate.substring(6, 10) + "-" + localKeyDate.substring(3, 5) + "-" + localKeyDate.substring(0, 2) +
							"T00:00:00";
					}
				}
				if (localRvadt) {
					if (localRvadt.substring(0, 2).includes(".")) {
						localRvadt = localRvadt.substring(5, 9) + "-" + localRvadt.substring(2, 4) + "-0" + localRvadt.substring(0, 1) + "T00:00:00";
					} else {
						localRvadt = localRvadt.substring(6, 10) + "-" + localRvadt.substring(3, 5) + "-" + localRvadt.substring(0, 2) + "T00:00:00";
					}
				}
				var localZterm = that.getView().byId("PaymentCondition").getSelectedKey();
				var localZlsch = that.getView().byId("PaymentKind").getSelectedKey();
				var localXref2 = that.getView().byId("ReferenceHeaderKey").getValue();
				var localSperr = that.getView().byId("checkBoxSperr").getSelected();
				var localZahls = that.getView().byId("checkBoxZahls").getSelected();
				var localBorcBak = that.getView().byId("checkBoxBorcBak").getSelected();
				var oneriListesiDetayModel = new JSONModel();
				if (globalSelectedNYTs.length !== 0) {
					that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.None);
					var localFdgrv = "Fdgrv eq ";
					for (var i = 0; i < globalSelectedNYTs.length; i++) {
						if (i === 0) {
							localFdgrv = localFdgrv + "'" + globalSelectedNYTs[i].getKey() + "'";
						} else {
							localFdgrv = localFdgrv + " or Fdgrv eq '" + globalSelectedNYTs[i].getKey() + "'";
						}
					}
					if (localSperr) {
						localSperr = "X";
					} else {
						localSperr = "";
					}
					if (localZahls) {
						localZahls = "X";
					} else {
						localZahls = "";
					}
					if (localBorcBak) {
						localBorcBak = "X";
					} else {
						localBorcBak = "";
					}
					if (localXref2 && (that.getView().byId("checkBoxWithoutRHK").getSelected() || that.getView().byId("checkBoxWithRHK").getSelected())) {
						if (that.getView().byId("checkBoxWithoutRHK").getSelected()) {
							oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
								globalSelectedCompanyCode +
								"' and Currency eq '" + localPB + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
								"' and (" + localFdgrv + ") and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
								"' and Xref2 ne '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" +
								localBorcBak +
								"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
						}
						if (that.getView().byId("checkBoxWithRHK").getSelected()) {
							oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
								globalSelectedCompanyCode +
								"' and Currency eq '" + localPB + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
								"' and (" + localFdgrv + ") and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
								"' and Xref2 eq '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" +
								localBorcBak +
								"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
						}
					} else {
						oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" + globalSelectedCompanyCode +
							"' and Currency eq '" + localPB + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
							"' and (" + localFdgrv + ") and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
							"' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" + localBorcBak +
							"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
					}
					oneriListesiDetayModel.attachRequestCompleted(function onComplete(oEvent) {
						if (oEvent.getParameter("success")) {
							if (digit === 1) {
								setTimeout(sap.ui.core.BusyIndicator.hide(), 10000);
								setTimeout(that.getView().byId("oneriListesi").setBusy(false), 10000);
							}
							globalOneriListesiDetayData = oEvent.getSource().getData().d.results;
							if (globalOneriListesiDetayData.length !== 0) {
								for (var j = 0; j < globalOneriListesiDetayData.length; j++) {
									if (globalOneriListesiDetayData[j].Rvadt !== undefined) {
										globalOneriListesiDetayData[j].Rvadt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[j].Rvadt);
									}
									if (globalOneriListesiDetayData[j].Vadet !== undefined) {
										globalOneriListesiDetayData[j].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[j].Vadet);
									}
									if (globalOneriListesiDetayData[j].Isltr !== undefined) {
										globalOneriListesiDetayData[j].Isltr = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[j].Isltr);
									}
									if (globalOneriListesiDetayData[j].Islst !== undefined) {
										globalOneriListesiDetayData[j].Islst = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").Time(
											globalOneriListesiDetayData[j].Islst);
									}
									var localAmountDifference = parseFloat(globalOneriListesiDetayData[j].VadeDoccur, 10) - parseFloat(
										globalOneriListesiDetayData[j].Odtut, 10);
									globalOneriListesiDetayData[j].Fark = localAmountDifference.toString();
								}
								var cModel = new JSONModel(globalOneriListesiDetayData);
								that.getView().setModel(cModel, "oneriListesiDetayList");
							} else {
								if (digit !== 1) {
									MessageBox.alert("Girilen değelere uygun kayıt bulunamadı!");
									that.getView().byId("oneriListesi").setNoData("Girilen değelere uygun kayıt bulunamadı!");
								}
							}
							that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.None);
							that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
						}
					});
				} else {
					if (globalAccountCodeKeys.length === 0) {
						var checkAccountCodeModel = new JSONModel();
						checkAccountCodeModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/ShSaticiSet?$filter=Bukrs eq '" + globalSelectedCompanyCode +
							"' and Lifnr eq '" + that.getView().byId("accountCode").getValue() + "'");
						checkAccountCodeModel.attachRequestCompleted(function onComplete(oEvent) {
							if (oEvent.getParameter("success")) {
								var checkAccountCodeData = [];
								checkAccountCodeData = oEvent.getSource().getData().d.results;
								if (checkAccountCodeData.length !== 0) {
									that.getView().byId("accountCode").setValue(that.getView().byId("accountCode").getValue() + " - " + checkAccountCodeData[0]
										.Name1);
									var localLifnr = that.getView().byId("accountCode").getValue();
									//var localLifnr = globalAccountCodeKey.toString();
									that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
									if (localSperr) {
										localSperr = "X";
									} else {
										localSperr = "";
									}
									if (localZahls) {
										localZahls = "X";
									} else {
										localZahls = "";
									}
									if (localBorcBak) {
										localBorcBak = "X";
									} else {
										localBorcBak = "";
									}
									if (localXref2 && (that.getView().byId("checkBoxWithoutRHK").getSelected() || that.getView().byId("checkBoxWithRHK").getSelected())) {
										if (that.getView().byId("checkBoxWithoutRHK").getSelected()) {
											oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
												globalSelectedCompanyCode +
												"' and Lifnr eq '" + localLifnr + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" +
												localRvadt +
												"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
												"' and Xref2 ne '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls +
												"' and Borcbak eq '" + localBorcBak +
												"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
										}
										if (that.getView().byId("checkBoxWithRHK").getSelected()) {
											oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
												globalSelectedCompanyCode +
												"' and Lifnr eq '" + localLifnr + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" +
												localRvadt +
												"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
												"' and Xref2 eq '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls +
												"' and Borcbak eq '" + localBorcBak +
												"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
										}
									} else {
										oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
											globalSelectedCompanyCode +
											"' and Lifnr eq '" + localLifnr + "' and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
											"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
											"' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" + localBorcBak +
											"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
									}
									oneriListesiDetayModel.attachRequestCompleted(function onComplete(oEvent) {
										if (oEvent.getParameter("success")) {
											if (digit === 1) {
												setTimeout(sap.ui.core.BusyIndicator.hide(), 10000);
												setTimeout(that.getView().byId("oneriListesi").setBusy(false), 10000);
											}
											globalOneriListesiDetayData = oEvent.getSource().getData().d.results;
											if (globalOneriListesiDetayData.length !== 0) {
												for (var i = 0; i < globalOneriListesiDetayData.length; i++) {
													globalOneriListesiDetayData[i].Rvadt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
														globalOneriListesiDetayData[i].Rvadt);
													globalOneriListesiDetayData[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
														globalOneriListesiDetayData[i].Vadet);
													globalOneriListesiDetayData[i].Isltr = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
														globalOneriListesiDetayData[i].Isltr);
													globalOneriListesiDetayData[i].Islst = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").Time(
														globalOneriListesiDetayData[i].Islst);
													var localAmountDifference = parseFloat(globalOneriListesiDetayData[i].VadeDoccur, 10) - parseFloat(
														globalOneriListesiDetayData[i].Odtut, 10);
													globalOneriListesiDetayData[i].Fark = localAmountDifference.toString();
													var cModel = new JSONModel(globalOneriListesiDetayData);
													that.getView().setModel(cModel, "oneriListesiDetayList");
												}
											} else {
												if (digit !== 1) {
													MessageBox.alert("Girilen değelere uygun kayıt bulunamadı!");
													that.getView().byId("oneriListesi").setNoData("Girilen değelere uygun kayıt bulunamadı!");
												}
											}
											that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.None);
											that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
										}
									});
								} else {
									if (that.getView().byId("NYT").getEditable()) {
										that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.Error);
									}
									if (that.getView().byId("accountCode").getEditable()) {
										that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
									}
									MessageBox.alert("Lütfen gerekli alanları doldurunuz!");
									sap.ui.core.BusyIndicator.hide();
									that.getView().byId("oneriListesi").setBusy(false);
								}
							} else {
								if (that.getView().byId("NYT").getEditable()) {
									that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.Error);
								}
								if (that.getView().byId("accountCode").getEditable()) {
									that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
								}
								MessageBox.alert("Lütfen gerekli alanları doldurunuz!");
								sap.ui.core.BusyIndicator.hide();
								that.getView().byId("oneriListesi").setBusy(false);
							}
						});
						// }

					} else {
						that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
						var localLifnr = "Lifnr eq ";
						for (var j = 0; j < globalAccountCodeKeys.length; j++) {
							if (j === 0) {
								localLifnr = localLifnr + "'" + globalAccountCodeKeys[j] + "'";
							} else {
								localLifnr = localLifnr + " or Lifnr eq '" + globalAccountCodeKeys[j] + "'";
							}
						}
						if (localSperr) {
							localSperr = "X";
						} else {
							localSperr = "";
						}
						if (localZahls) {
							localZahls = "X";
						} else {
							localZahls = "";
						}
						if (localBorcBak) {
							localBorcBak = "X";
						} else {
							localBorcBak = "";
						}
						if (localXref2 && (that.getView().byId("checkBoxWithoutRHK").getSelected() || that.getView().byId("checkBoxWithRHK").getSelected())) {
							if (that.getView().byId("checkBoxWithoutRHK").getSelected()) {
								oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
									globalSelectedCompanyCode +
									"' and (" + localLifnr + ") and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
									"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
									"' and Xref2 ne '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" +
									localBorcBak +
									"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
							}
							if (that.getView().byId("checkBoxWithRHK").getSelected()) {
								oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
									globalSelectedCompanyCode +
									"' and (" + localLifnr + ") and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
									"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
									"' and Xref2 eq '" + localXref2 + "' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" +
									localBorcBak +
									"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
							}
						} else {
							oneriListesiDetayModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/DetayOneriListSet?$filter=Bukrs eq '" +
								globalSelectedCompanyCode +
								"' and (" + localLifnr + ") and KeyDate eq datetime'" + localKeyDate + "' and Rvadt le datetime'" + localRvadt +
								"' and Currency eq '" + localPB + "' and Zterm eq '" + localZterm + "' and Zlsch eq '" + localZlsch +
								"' and Sperr eq '" + localSperr + "' and Zahls eq '" + localZahls + "' and Borcbak eq '" + localBorcBak +
								"' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() + "'");
						}
						oneriListesiDetayModel.attachRequestCompleted(function onComplete(oEvent) {
							if (oEvent.getParameter("success")) {
								if (digit === 1) {
									setTimeout(sap.ui.core.BusyIndicator.hide(), 10000);
									setTimeout(that.getView().byId("oneriListesi").setBusy(false), 10000);
								}
								globalOneriListesiDetayData = oEvent.getSource().getData().d.results;
								if (globalOneriListesiDetayData.length !== 0) {
									for (var i = 0; i < globalOneriListesiDetayData.length; i++) {
										globalOneriListesiDetayData[i].Rvadt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[i].Rvadt);
										globalOneriListesiDetayData[i].Vadet = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[i].Vadet);
										globalOneriListesiDetayData[i].Isltr = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(
											globalOneriListesiDetayData[i].Isltr);
										globalOneriListesiDetayData[i].Islst = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").Time(
											globalOneriListesiDetayData[i].Islst);
										var localAmountDifference = parseFloat(globalOneriListesiDetayData[i].VadeDoccur, 10) - parseFloat(
											globalOneriListesiDetayData[i].Odtut, 10);
										globalOneriListesiDetayData[i].Fark = localAmountDifference.toString();
									}
									var cModel = new JSONModel(globalOneriListesiDetayData);
									that.getView().setModel(cModel, "oneriListesiDetayList");
								} else {
									if (digit !== 1) {
										MessageBox.alert("Girilen değelere uygun kayıt bulunamadı!");
										that.getView().byId("oneriListesi").setNoData("Girilen değelere uygun kayıt bulunamadı!");
									}

								}
								that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.None);
								that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.None);
							}
						});
					}
				}
				if (digit !== 1) {
					// sap.ui.core.BusyIndicator.show();
					that.getView().byId("oneriListesi").setBusy(true);
					that.getTableDatas(1);
				}
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("SecondRegister").setVisible(true);
				// that.getView().byId("allParameters-CollapsedImg-img").attachPress(that.checkExpanded, that);
				that.getView().byId("allParameters")._oExpandButton.attachPress(that.checkExpanded, that);
			} else {
				that.getView().byId("CompanyCode").setValueState(sap.ui.core.ValueState.Error);
				that.getView().byId("OpenPaymentDate").setValueState(sap.ui.core.ValueState.Error);
				that.getView().byId("ReferenceDate").setValueState(sap.ui.core.ValueState.Error);
				that.getView().byId("Currency").setValueState(sap.ui.core.ValueState.Error);
				if (that.getView().byId("NYT").getEditable()) {
					that.getView().byId("NYT").setValueState(sap.ui.core.ValueState.Error);
				}
				if (that.getView().byId("accountCode").getEditable()) {
					that.getView().byId("accountCode").setValueState(sap.ui.core.ValueState.Error);
				}
				MessageBox.alert("Lütfen gerekli alanları doldurunuz!");
				sap.ui.core.BusyIndicator.hide();
				that.getView().byId("oneriListesi").setBusy(false);
			}
		},
		
		checkExpanded: function (oEvent) {
			var that = this;
			if (that.getView().byId("allParameters").getExpanded()) {
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("oneriListesi").setVisibleRowCount(15);
				that.getView().byId("SecondRegister").setVisible(true);
			}
			if (!that.getView().byId("allParameters").getExpanded()) {
				that.getView().byId("oneriListesi").setVisibleRowCount(6);
				that.getView().byId("oneriListesi").setVisibleRowCount(6);
				that.getView().byId("oneriListesi").setVisibleRowCount(6);
				that.getView().byId("SecondRegister").setVisible(false);
			}
		},

		handleValueHelp: function (oEvent) {
			var sInputValue = oEvent.getSource().getValue();

			this.inputId = oEvent.getSource().getId();
			// create value help dialog
			if (!this.valueHelpDialog) {
				this.valueHelpDialog = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.AccountCodeDialogMulti", this);
				// this.valueHelpDialog = sap.ui.xmlfragment("MDP_E-Odeme.FIEodeme.view.AccountCodeDialog", this);
				this.getView().addDependent(this.valueHelpDialog);
			}

			if (sInputValue.length > 4) {
				// create a filter for the binding
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Lifnr", sap.ui.model.FilterOperator.Contains, sInputValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sInputValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				this.valueHelpDialog.getBinding("items").filter(secondFilter);
			}

			// open value help dialog filtered by the input value
			this.valueHelpDialog.open(sInputValue);
		},

		handleValueHelpSearch: function (evt) {
			var sValue = evt.getParameter("value");
			if (sValue.length > 4) {
				var firstFilter = new sap.ui.model.Filter({
					or: true,
					filters: [
						new Filter("Lifnr", sap.ui.model.FilterOperator.Contains, sValue),
						new Filter("Name1", sap.ui.model.FilterOperator.Contains, sValue)
					]
				});
				var secondFilter = new sap.ui.model.Filter({
					and: true,
					filters: [
						new Filter("Bukrs", sap.ui.model.FilterOperator.EQ, globalSelectedCompanyCode),
						firstFilter
					]
				});
				evt.getSource().getBinding("items").filter(secondFilter);
			}
		},

		handleValueHelpClose: function (evt) {
			var additionControlFlag;
			var oMultiInput = this.byId("accountCode");
			this.getView().byId("accountCode").setValue("");
			var aSelectedItems = evt.getParameter("selectedItems");
			if (aSelectedItems && aSelectedItems.length > 0) {
				var counter = 0;
				aSelectedItems.forEach(function (oItem) {
					additionControlFlag = 0;
					for (var counter2 = 0; counter2 < globalAccountCodeKeys.length; counter2++) {
						if (globalAccountCodeKeys[counter2].toString() === aSelectedItems[counter].getBindingInfo("title").binding.aOriginalValues[0].toString()) {
							additionControlFlag = 1;
							break;
						}
					}
					if (additionControlFlag === 0) {
						globalAccountCodeKeys.push(aSelectedItems[counter].getBindingInfo("title").binding.aOriginalValues[0]);
						globalAccountCodeTitles.push(oItem.getTitle());
						oMultiInput.addToken(new Token({
							key: aSelectedItems[counter].getBindingInfo("title").binding.aOriginalValues[0].toString(),
							// text: oItem.getTitle()
							text: aSelectedItems[counter].getBindingInfo("title").binding.aOriginalValues[0].toString()
						}));
					}
					counter++;
				});
			}
		},

		onTokenUpdate: function (oEvent) {
			var sType = oEvent.getParameter("type");
			if (sType === "removed") {
				var sKey = oEvent.getParameter("removedTokens")[0].getProperty("key");
				for (var i = 0; i < globalAccountCodeKeys.length; i++) {
					if (globalAccountCodeKeys[i].toString() === sKey.toString()) {
						globalAccountCodeKeys.splice(i, 1);
						globalAccountCodeTitles.splice(i, 1);
					}
				}
			}
		},

		suggestionItemSelected: function (evt) {
			var oItem = evt.getParameter('selectedItem'),
				oText = this.byId('selectedKey'),
				sKey = oItem ? oItem.getKey() : '';
			oText.setText(sKey);
		},

		calculateTotalAmount: function () {
			this.getView().byId("SimpleFormChange480_12120-2").toggleStyleClass("SimpleFormChange480_12120-2");
			var totalVadeDoccur = 0;
			var selectedIndices = [];
			var totalVadeDoccurCurrency = "";
			var table = this.getView().byId("oneriListesi");
			selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("oneriListesiDetayList");
			var totalList = {
				"cost": "0,00",
				"pb": " "
			};
			if (selectedIndices.length !== 0) {
				totalVadeDoccurCurrency = model.getData()[selectedIndices[0]].Currency;
				for (var i = 0; i < selectedIndices.length; i++) {
					totalVadeDoccur = totalVadeDoccur + parseFloat(model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Odtut, 10);
				}
				totalList.cost = totalVadeDoccur.toString();
				totalList.pb = totalVadeDoccurCurrency;
			}
			var costModel = new JSONModel(totalList);
			costModel.setProperty("/dataList", totalList);
			this.getView().setModel(costModel, "costData");
			for (var j = 0; j < globalOneriListesiDetayData.length; j++) {
				var localAmountDifference = parseFloat(globalOneriListesiDetayData[j].VadeDoccur, 10) - parseFloat(globalOneriListesiDetayData[j].Odtut,
					10);
				globalOneriListesiDetayData[j].Fark = localAmountDifference.toString();
			}
			var cModel = new JSONModel(globalOneriListesiDetayData);
			this.getView().setModel(cModel, "oneriListesiDetayList");
			this.getView().byId("SimpleFormChange480_12120-2").toggleStyleClass("SimpleFormChange480_12120-2BigAndColorful");
		},

		saveAndReturn: function (oEvent) {
			var totalVadeDoccur = 0;
			var table = this.getView().byId("oneriListesi");
			var selectedIndices = table.getSelectedIndices();
			var model = this.getView().getModel("oneriListesiDetayList");
			globalSelectedIndices = [];
			for (var i = 0; i < selectedIndices.length; i++) {
				var tmpRow = {
					"Currency": "",
					"Gsber": "",
					"Lifnr": "",
					"Bukrs": "",
					"Gjahr": "",
					"Fdgrv": "",
					"Sperr": "",
					"Zahls": "",
					"KeyDate": "",
					"Rvadt": "",
					"Zterm": "",
					"Zwels": "",
					"Xref2": "",
					"Name1": "",
					"Stcd2": "",
					"Textl": "",
					"AmtDoccur": "",
					"VadeDoccur": "",
					"LcAmount": "",
					"LocCurrcy": "",
					"Vadet": "",
					"Isltr": "",
					"Islst": "",
					"Usnam": "",
					"Pernr": "",
					"Odtut": ""
				};
				tmpRow.Currency = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Currency;
				tmpRow.Gsber = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Gsber;
				tmpRow.Lifnr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Lifnr;
				tmpRow.Bukrs = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Bukrs;
				tmpRow.Gjahr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Gjahr;
				tmpRow.Fdgrv = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Fdgrv;
				tmpRow.Sperr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Sperr;
				tmpRow.Zahls = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Zahls;
				tmpRow.KeyDate = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate(model.getData()[table.getBinding("rows")
					.aIndices[selectedIndices[i]]].KeyDate);
				if (tmpRow.KeyDate != undefined) {
					if (tmpRow.KeyDate.substring(0, 2).includes(".")) {
						tmpRow.KeyDate = tmpRow.KeyDate.substring(5, 9) + "-" + tmpRow.KeyDate.substring(2, 4) + "-0" + tmpRow.KeyDate.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.KeyDate = tmpRow.KeyDate.substring(6, 10) + "-" + tmpRow.KeyDate.substring(3, 5) + "-" + tmpRow.KeyDate.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Rvadt = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Rvadt;
				if (tmpRow.Rvadt != undefined) {
					if (tmpRow.Rvadt.substring(0, 2).includes(".")) {
						tmpRow.Rvadt = tmpRow.Rvadt.substring(5, 9) + "-" + tmpRow.Rvadt.substring(2, 4) + "-0" + tmpRow.Rvadt.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Rvadt = tmpRow.Rvadt.substring(6, 10) + "-" + tmpRow.Rvadt.substring(3, 5) + "-" + tmpRow.Rvadt.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Zterm = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Zterm;
				tmpRow.Zwels = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Zwels;
				tmpRow.Xref2 = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Xref2;
				tmpRow.Name1 = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Name1;
				tmpRow.Stcd2 = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Stcd2;
				tmpRow.Textl = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Textl;
				tmpRow.AmtDoccur = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].AmtDoccur;
				tmpRow.VadeDoccur = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].VadeDoccur;
				totalVadeDoccur = totalVadeDoccur + parseFloat(model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Odtut, 10);
				tmpRow.LcAmount = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].LcAmount;
				tmpRow.LocCurrcy = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].LocCurrcy;
				tmpRow.Vadet = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Vadet;
				if (tmpRow.Vadet !== undefined) {
					if (tmpRow.Vadet.substring(0, 2).includes(".")) {
						tmpRow.Vadet = tmpRow.Vadet.substring(5, 9) + "-" + tmpRow.Vadet.substring(2, 4) + "-0" + tmpRow.Vadet.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Vadet = tmpRow.Vadet.substring(6, 10) + "-" + tmpRow.Vadet.substring(3, 5) + "-" + tmpRow.Vadet.substring(0, 2) +
							"T00:00:00";
					}
				}
				tmpRow.Isltr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Isltr;
				if (tmpRow.Isltr !== undefined) {
					if (tmpRow.Isltr.substring(0, 2).includes(".")) {
						tmpRow.Isltr = tmpRow.Isltr.substring(5, 9) + "-" + tmpRow.Isltr.substring(2, 4) + "-0" + tmpRow.Isltr.substring(0, 1) +
							"T00:00:00";
					} else {
						tmpRow.Isltr = tmpRow.Isltr.substring(6, 10) + "-" + tmpRow.Isltr.substring(3, 5) + "-" + tmpRow.Isltr.substring(0, 2) +
							"T00:00:00";
					}
				}
				if (model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Islst !== undefined) {
					tmpRow.Islst = "PT" + model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Islst.substring(0, 2) + "H" +
						model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Islst.substring(3, 5) + "M" + model.getData()[
							selectedIndices[i]].Islst.substring(6, 8) + "S";
				}
				tmpRow.Usnam = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Usnam;
				tmpRow.Pernr = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Pernr;
				tmpRow.Odtut = model.getData()[table.getBinding("rows").aIndices[selectedIndices[i]]].Odtut.toString();
				globalSelectedIndices[i] = tmpRow;
			}

			totalVadeDoccur = Math.abs(totalVadeDoccur);
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setPaymentAmount(totalVadeDoccur);
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setChoosenAmount(totalVadeDoccur);
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").setCurrency(tmpRow.Currency);
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Odeme_Talebi_Girisi");
		},

		getGlobalSelectedIndicesOneriList: function () {
			return globalSelectedIndices;
		},

		unchekAC: function (oEvent) {
			var that = this;
			that.getView().byId("checkBoxAC").setSelected(false);
			globalAccountCodeKeys = [];
			globalAccountCodeTitles = [];
			that.byId("accountCode").removeAllTokens();
			// globalAccountCodeKey = "";
			that.getView().byId("accountCode").setEditable(false);
			that.getView().byId("accountCode").setValue("");
			that.getView().byId("checkBoxNYT").setSelected(true);
			that.getView().byId("NYT").setEditable(true);
		},

		unchekNYT: function (oEvent) {
			var that = this;
			that.getView().byId("checkBoxNYT").setSelected(false);
			globalSelectedNYTs = [];
			that.getView().byId("NYT").setEditable(false);
			that.getView().byId("NYT").setSelectedItems([]);
			that.getView().byId("NYT").setSelectedKeys([]);
			that.getView().byId("checkBoxAC").setSelected(true);
			that.getView().byId("accountCode").setEditable(true);
		},

		unchekWithoutRHK: function (oEvent) {
			var that = this;
			if (that.getView().byId("checkBoxWithoutRHK").getSelected() && that.getView().byId("checkBoxWithRHK").getSelected()) {
				that.getView().byId("checkBoxWithoutRHK").setSelected(false);
				that.getView().byId("checkBoxWithRHK").setSelected(true);
			}
		},

		unchekWithRHK: function (oEvent) {
			var that = this;
			if (that.getView().byId("checkBoxWithoutRHK").getSelected() && that.getView().byId("checkBoxWithRHK").getSelected()) {
				that.getView().byId("checkBoxWithoutRHK").setSelected(true);
				that.getView().byId("checkBoxWithRHK").setSelected(false);
			}
		},

		goPaymentEntryPage: function (oEvent) {
			this.clearAllFields();
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Odeme_Talebi_Girisi");
			// window.location.reload();
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							that.clearAllFields();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi.
								_oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		detailSuggestedItem: function (oEvent) {
			var that = this;
			var localLifnr = globalOneriListesiDetayData[oEvent.getSource().getParent().getIndex()].Lifnr;
			var localPB = globalOneriListesiDetayData[oEvent.getSource().getParent().getIndex()].Currency;
			var localGsber = globalOneriListesiDetayData[oEvent.getSource().getParent().getIndex()].Gsber;
			var oButton = oEvent.getSource();
			globalDetailSuggestedItemSource = oButton;
			if (!this._oQuickView) {
				Fragment.load({
					name: "MDP_E-Odeme.FIEodeme.view.detailSuggestedItem",
					controller: this
				}).then(function (oQuickView) {
					var oneriListesiDetayItemModel = new JSONModel();
					oneriListesiDetayItemModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OneriDetaySet?$filter=Lifnr eq '" + localLifnr +
						"'  and Waers eq '" + localPB +
						"' and Gsber eq '" + localGsber + "' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() +
						"'");
					oneriListesiDetayItemModel.attachRequestCompleted(function onComplete(oEvent2) {
						if (oEvent2.getParameter("success")) {
							var oneriListesiDetayItemData = oEvent2.getSource().getData().d.results;
							oneriListesiDetayItemModel.setProperty("/itemList", oneriListesiDetayItemData);
							that.calculateTotalAmountOfSubPayments(oneriListesiDetayItemData);
							oQuickView.setModel(oneriListesiDetayItemModel, "item");
						}
					});
					oQuickView.openBy(oButton);
					globalQuickView = oQuickView;
				});
			} else {
				this.getView().addDependent(this._oQuickView);
				this._oQuickView.openBy(oButton);
			}
		},

		closePopUp: function (oEvent) {
			globalQuickView.close();
			globalQuickView = null;
		},

		exportExcel: function (e) {
			this.exportExcelSheet("oneriListesi", "oneriListesiDetayList", "", "Öneri Listesinden Seçilen Kalemler");
		},

		exportExcelSheet: function (tableId, modelName, property, fileName) {
			var view = this.getView();
			var table = view.byId(tableId); //parametre olarak tanımlanan tablonun bilgisi
			var selectedIndices = table.getSelectedIndices(); //tablodaki seçili indexler
			var data = [];
			var model = new JSONModel();
			var columns = table.getColumns(); //tablonun columnlarını alır.
			var exportColumns = [];
			var costModel = this.getView().getModel("costData");
			var indexOfPointCostModelODataCost = -1;
			indexOfPointCostModelODataCost = costModel.oData.cost.toString().indexOf('.');
			var totalCost = {
				AmtDoccur: "",
				Borcbak: "",
				Bukrs: "",
				Currency: "",
				Fdgrv: "",
				Gjahr: "",
				Gsber: "",
				Islst: "",
				Isltr: "",
				KeyDate: "",
				LcAmount: "",
				Lifnr: "",
				LocCurrcy: "",
				Name1: "",
				Pernr: "",
				Rvadt: "",
				Sperr: "",
				Stcd2: "",
				Textl: "Toplam Tutar:",
				Usnam: "",
				VadeDoccur: costModel.oData.pb.toString(),
				Vadet: "",
				Xref2: "",
				Zahls: "",
				Zlsch: "",
				Zterm: "",
				Zwels: "",
				Odtut: "",
				Fark: ""
			};
			if (indexOfPointCostModelODataCost === -1) {
				totalCost.Odtut = costModel.oData.cost.toString() + ".00";
			} else {
				totalCost.Odtut = costModel.oData.cost.toString().substring(0, (indexOfPointCostModelODataCost + 3));
			}
			var emptyRow = {
				AmtDoccur: "",
				Borcbak: "",
				Bukrs: "",
				Currency: "",
				Fdgrv: "",
				Gjahr: "",
				Gsber: "",
				Islst: "",
				Isltr: "",
				KeyDate: "",
				LcAmount: "",
				Lifnr: "",
				LocCurrcy: "",
				Name1: "",
				Pernr: "",
				Rvadt: "",
				Sperr: "",
				Stcd2: "",
				Textl: "",
				Usnam: "",
				VadeDoccur: "",
				Vadet: "",
				Xref2: "",
				Zahls: "",
				Zlsch: "",
				Zterm: "",
				Zwels: "",
				Odtut: "",
				Fark: ""
			};
			for (var i = 0; i < (selectedIndices.length + 2); i++) {
				var singleData;
				if (i === selectedIndices.length + 1) {
					singleData = totalCost;
				} else if (i === selectedIndices.length) {
					singleData = emptyRow;
				} else {
					singleData = view.getModel(modelName).getProperty("/" + table.getBinding("rows").aIndices[selectedIndices[i]]);
				}
				var obj = $.extend(true, {}, singleData); //referanstan kurtarmak için kullanılır.
				if (i < selectedIndices.length) {
					var indexOfPointObjFark = -1;
					indexOfPointObjFark = obj.Fark.toString().indexOf('.');
					if (indexOfPointObjFark === -1) {
						obj.Fark = obj.Fark.toString() + ".00";
					} else {
						obj.Fark = obj.Fark.toString().substring(0, (indexOfPointObjFark + 3));
					}
					var indexOfPointObjOdtut = -1;
					indexOfPointObjOdtut = obj.Odtut.toString().indexOf('.');
					if (indexOfPointObjOdtut === -1) {
						obj.Odtut = obj.Odtut.toString() + ".00";
					} else {
						obj.Odtut = obj.Odtut.toString().substring(0, (indexOfPointObjOdtut + 3));
					}
				}
				if (Object.entries(obj).length > 0) {
					data.push(obj);
				}
			}
			model.setProperty("/dataList", data);
			var path;
			for (var j = 0; j < columns.length; j++) {
				if (columns[j].getTemplate().getBindingInfo("text")) { // tablodaki veriler ve yolları geliyor.
					path = columns[j].getTemplate().getBindingInfo("text").parts[0].path; //columndaki bilgi geliyor.Cari Hesap vs.
				} else if (columns[j].getTemplate().getBindingInfo("number")) {
					path = columns[j].getTemplate().getBindingInfo("number").parts[0].path;
				} else if (columns[j].getTemplate().getBindingInfo("value")) {
					path = columns[j].getTemplate().getBindingInfo("value").parts[0].path;
				} else {
					path = columns[j].getTemplate().getBindingInfo("text");
				}
				if (columns[j].getLabel().getText() !== "Detay") {
					var temp = {
						name: columns[j].getLabel().getText(),
						template: {
							content: "{" + path + "}"
						}
					};
					exportColumns.push(temp);
				}
			}
			var oExport = new Export({
				exportType: new ExportTypeCSV({
					fileExtension: "csv",
					separatorChar: ";"
				}),
				models: model,
				rows: {
					path: "/dataList"
				},
				columns: exportColumns
			});
			oExport.saveFile(fileName).catch(function (error) {}).then(function () {
				oExport.destroy();
			});
		},

		onSave: function (oEvent) {
			var that = this;
			jQuery.sap.require("sap.m.MessageToast");
			var params = oEvent.getParameters();
			if (params.overwrite) {
				var parametersValue = this.getParametersValue();
				var save = Object.create(null);
				save.CompanyCode = parametersValue.CompanyCode;
				save.globalSelectedCompanyCode = parametersValue.globalSelectedCompanyCode;
				save.OpenPaymentDate = parametersValue.OpenPaymentDate;
				save.ReferenceDate = parametersValue.ReferenceDate;
				save.Currency = parametersValue.Currency;
				save.NYT = parametersValue.NYT;
				save.globalSelectedNYTs = [];
				save.globalSelectedNYTs = parametersValue.globalSelectedNYTs;
				save.checkBoxNYT = parametersValue.checkBoxNYT;
				save.accountCode = parametersValue.accountCode;
				save.globalAccountCodeKeys = [];
				save.globalAccountCodeKeys = parametersValue.globalAccountCodeKeys;
				save.globalAccountCodeTitles = [];
				save.globalAccountCodeTitles = parametersValue.globalAccountCodeTitles;
				save.checkBoxAC = parametersValue.checkBoxAC;
				save.PaymentCondition = parametersValue.PaymentCondition;
				save.PaymentKind = parametersValue.PaymentKind;
				save.ReferenceHeaderKey = parametersValue.ReferenceHeaderKey;
				save.checkBoxWithoutRHK = parametersValue.checkBoxWithoutRHK;
				save.checkBoxWithRHK = parametersValue.checkBoxWithRHK;
				save.checkBoxSperr = parametersValue.checkBoxSperr;
				save.checkBoxZahls = parametersValue.checkBoxZahls;
				save.checkBoxBorcBak = parametersValue.checkBoxBorcBak;
				save.VAR_KEY = params.key;
				save.VAR_NAME = params.name;
				globalVariantItemsList.push(save);
				var cModel = new JSONModel(globalVariantItemsList);
				that.getView().setModel(cModel, "variantItemsList");

				var jsonHeader = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Datum": "0000-00-00T00:00:00",
					"NPVariant": []
				};

				var localItems = [];

				var jsonItem1 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "CompanyCode",
					"Value": save.CompanyCode.toString()
				};
				localItems.push(jsonItem1);

				var jsonItem2 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "globalSelectedCompanyCode",
					"Value": save.globalSelectedCompanyCode.toString()
				};
				localItems.push(jsonItem2);

				var jsonItem3 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "OpenPaymentDate",
					"Value": save.OpenPaymentDate.toString()
				};
				localItems.push(jsonItem3);

				var jsonItem4 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "ReferenceDate",
					"Value": save.ReferenceDate.toString()
				};
				localItems.push(jsonItem4);

				var jsonItem5 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "Currency",
					"Value": save.Currency.toString()
				};
				localItems.push(jsonItem5);

				var jsonItem6 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "NYT",
					"Value": save.NYT.toString()
				};
				localItems.push(jsonItem6);

				for (var i = 0; i < save.globalSelectedNYTs.length; i++) {
					var jsonItem7 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalSelectedNYTs",
						"Value": save.globalSelectedNYTs[i].getKey().toString()
					};
					localItems.push(jsonItem7);
				}

				var jsonItem8 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxNYT",
					"Value": save.checkBoxNYT.toString()
				};
				localItems.push(jsonItem8);

				var jsonItem9 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "accountCode",
					"Value": save.accountCode.toString()
				};
				localItems.push(jsonItem9);

				for (var i = 0; i < save.globalAccountCodeKeys.length; i++) {
					var jsonItem10 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalAccountCodeKeys",
						"Value": save.globalAccountCodeKeys[i].toString()
					};
					localItems.push(jsonItem10);
				}

				var jsonItem11 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxAC",
					"Value": save.checkBoxAC.toString()
				};
				localItems.push(jsonItem11);

				var jsonItem12 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "PaymentCondition",
					"Value": save.PaymentCondition.toString()
				};
				localItems.push(jsonItem12);

				var jsonItem13 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "PaymentKind",
					"Value": save.PaymentKind.toString()
				};
				localItems.push(jsonItem13);

				var jsonItem14 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "ReferenceHeaderKey",
					"Value": save.ReferenceHeaderKey.toString()
				};
				localItems.push(jsonItem14);

				var jsonItem15 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxWithoutRHK",
					"Value": save.checkBoxWithoutRHK.toString()
				};
				localItems.push(jsonItem15);

				var jsonItem16 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxWithRHK",
					"Value": save.checkBoxWithRHK.toString()
				};
				localItems.push(jsonItem16);

				var jsonItem17 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxSperr",
					"Value": save.checkBoxSperr.toString()
				};
				localItems.push(jsonItem17);

				var jsonItem18 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxZahls",
					"Value": save.checkBoxZahls.toString()
				};
				localItems.push(jsonItem18);

				var jsonItem19 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxBorcBak",
					"Value": save.checkBoxBorcBak.toString()
				};
				localItems.push(jsonItem19);

				for (var i = 0; i < save.globalAccountCodeTitles.length; i++) {
					var jsonItem20 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalAccountCodeTitles",
						"Value": save.globalAccountCodeTitles[i].toString()
					};
					localItems.push(jsonItem20);
				}

				jsonHeader.NPVariant = localItems;

				that.createModel = new JSONModel(jsonHeader);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();
				var oModel = this.getOwnerComponent().getModel();
				oModel.create("/VariantHeaderSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						sap.m.MessageToast.show("'" + params.name + "' ismindeki varyantınız başarılı bir şekilde güncellenmiştir!");
						oModel.refresh();
					},
					error: function (err) {
						sap.m.MessageToast.show("Varyant güncelleme başarısız!");
					}
				});

				// $.extend( modelData, save );
				// that.getView().getModel().refresh();                      
				// oDataModel.update("DetayOneriListSet('"+save.VAR_KEY+"')", save, null,function(oData, response){
				// 	}, function(err) {
				// 	MessageBox.alert("Service Failed");
				// });
			} else {
				var parametersValue = this.getParametersValue();
				var newEntry = Object.create(null);
				newEntry.VAR_NAME = params.name;
				newEntry.VAR_KEY = params.key;
				newEntry.CompanyCode = parametersValue.CompanyCode;
				newEntry.globalSelectedCompanyCode = parametersValue.globalSelectedCompanyCode;
				newEntry.OpenPaymentDate = parametersValue.OpenPaymentDate;
				newEntry.ReferenceDate = parametersValue.ReferenceDate;
				newEntry.Currency = parametersValue.Currency;
				newEntry.NYT = parametersValue.NYT;
				newEntry.globalSelectedNYTs = [];
				newEntry.globalSelectedNYTs = parametersValue.globalSelectedNYTs;
				newEntry.checkBoxNYT = parametersValue.checkBoxNYT;
				newEntry.accountCode = parametersValue.accountCode;
				newEntry.globalAccountCodeKeys = [];
				newEntry.globalAccountCodeKeys = parametersValue.globalAccountCodeKeys;
				newEntry.globalAccountCodeTitles = [];
				newEntry.globalAccountCodeTitles = parametersValue.globalAccountCodeTitles;
				newEntry.checkBoxAC = parametersValue.checkBoxAC;
				newEntry.PaymentCondition = parametersValue.PaymentCondition;
				newEntry.PaymentKind = parametersValue.PaymentKind;
				newEntry.ReferenceHeaderKey = parametersValue.ReferenceHeaderKey;
				newEntry.checkBoxWithoutRHK = parametersValue.checkBoxWithoutRHK;
				newEntry.checkBoxWithRHK = parametersValue.checkBoxWithRHK;
				newEntry.checkBoxSperr = parametersValue.checkBoxSperr;
				newEntry.checkBoxZahls = parametersValue.checkBoxZahls;
				newEntry.checkBoxBorcBak = parametersValue.checkBoxBorcBak;
				globalVariantItemsList.push(newEntry);
				var cModel = new JSONModel(globalVariantItemsList);
				that.getView().setModel(cModel, "variantItemsList");

				var jsonHeader = {
					"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr(),
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Datum": "0000-00-00T00:00:00",
					"NPVariant": []
				};

				var localItems = [];

				var jsonItem1 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "CompanyCode",
					"Value": newEntry.CompanyCode.toString()
				};
				localItems.push(jsonItem1);

				var jsonItem2 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "globalSelectedCompanyCode",
					"Value": newEntry.globalSelectedCompanyCode.toString()
				};
				localItems.push(jsonItem2);

				var jsonItem3 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "OpenPaymentDate",
					"Value": newEntry.OpenPaymentDate.toString()
				};
				localItems.push(jsonItem3);

				var jsonItem4 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "ReferenceDate",
					"Value": newEntry.ReferenceDate.toString()
				};
				localItems.push(jsonItem4);

				var jsonItem5 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "Currency",
					"Value": newEntry.Currency.toString()
				};
				localItems.push(jsonItem5);

				var jsonItem6 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "NYT",
					"Value": newEntry.NYT.toString()
				};
				localItems.push(jsonItem6);

				for (var i = 0; i < newEntry.globalSelectedNYTs.length; i++) {
					var jsonItem7 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalSelectedNYTs",
						"Value": newEntry.globalSelectedNYTs[i].getKey().toString()
					};
					localItems.push(jsonItem7);
				}

				var jsonItem8 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxNYT",
					"Value": newEntry.checkBoxNYT.toString()
				};
				localItems.push(jsonItem8);

				var jsonItem9 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "accountCode",
					"Value": newEntry.accountCode.toString()
				};
				localItems.push(jsonItem9);

				for (var i = 0; i < newEntry.globalAccountCodeKeys.length; i++) {
					var jsonItem10 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalAccountCodeKeys",
						"Value": newEntry.globalAccountCodeKeys[i].toString()
					};
					localItems.push(jsonItem10);
				}

				var jsonItem11 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxAC",
					"Value": newEntry.checkBoxAC.toString()
				};
				localItems.push(jsonItem11);

				var jsonItem12 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "PaymentCondition",
					"Value": newEntry.PaymentCondition.toString()
				};
				localItems.push(jsonItem12);

				var jsonItem13 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "PaymentKind",
					"Value": newEntry.PaymentKind.toString()
				};
				localItems.push(jsonItem13);

				var jsonItem14 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "ReferenceHeaderKey",
					"Value": newEntry.ReferenceHeaderKey.toString()
				};
				localItems.push(jsonItem14);

				var jsonItem15 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxWithoutRHK",
					"Value": newEntry.checkBoxWithoutRHK.toString()
				};
				localItems.push(jsonItem15);

				var jsonItem16 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxWithRHK",
					"Value": newEntry.checkBoxWithRHK.toString()
				};
				localItems.push(jsonItem16);

				var jsonItem17 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxSperr",
					"Value": newEntry.checkBoxSperr.toString()
				};
				localItems.push(jsonItem17);

				var jsonItem18 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxZahls",
					"Value": newEntry.checkBoxZahls.toString()
				};
				localItems.push(jsonItem18);

				var jsonItem19 = {
					"Variant": params.name,
					"Pname": "ONERILIST",
					"Field": "checkBoxBorcBak",
					"Value": newEntry.checkBoxBorcBak.toString()
				};
				localItems.push(jsonItem19);

				for (var i = 0; i < newEntry.globalAccountCodeTitles.length; i++) {
					var jsonItem20 = {
						"Variant": params.name,
						"Pname": "ONERILIST",
						"Field": "globalAccountCodeTitles",
						"Value": newEntry.globalAccountCodeTitles[i].toString()
					};
					localItems.push(jsonItem20);
				}

				jsonHeader.NPVariant = localItems;

				that.createModel = new JSONModel(jsonHeader);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();
				var oModel = this.getOwnerComponent().getModel();
				oModel.create("/VariantHeaderSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						sap.m.MessageToast.show("Varyantınız '" + params.name + "' ismi ile başarılı bir şekilde kaydedilmiştir!");
						oModel.refresh();
					},
					error: function (err) {
						sap.m.MessageToast.show("Varyant kaydetme başarısız!");
					}
				});

				//Updating database via Odata

				// oDataModel.create("DetayOneriListSet", newEntry, null, function(oData, response){
				// 	//Updating Json Model Local Data
				//                 var Data = that.getView().getModel().getData().VariantSet;
				// 	Data.push(newEntry);
				// 	that.getView().getModel().refresh();
				// 	}, function(err) {
				// 	MessageBox.alert("Service Failed");
				// });
			}
		},

		onManage: function (oEvent) {
			"use strict";
			jQuery.sap.require("sap.m.MessageToast");
			var params = oEvent.getParameters();
			var renamed = params.renamed;
			var deleted = params.deleted;
			var that = this;
			var sMessage;

			//rename backend data
			if (renamed) {
				for (var i = 0; i < renamed.length; i++) {
					for (var j = 0; j < globalVariantItemsList.length; j++) {
						if (globalVariantItemsList[j].VAR_KEY.toString() === renamed[i].key.toString()) {
							globalVariantItemsList[j].VAR_KEY = "ONERILIST" + renamed[i].name.toString();
							globalVariantItemsList[j].VAR_NAME = renamed[i].name.toString();
						}
					}
				}
				for (var i = 0; i < renamed.length; i++) {
					for (var j = 0; j < globalVariantItemsList.length; j++) {
						if (globalVariantItemsList[j].VAR_KEY.toString() === renamed[i].key.toString()) {
							globalVariantItemsList.splice(j, 1);
						}
					}
				}
				// renamed.forEach(function(rename){
				// 	oDataModel.update("/DetayOneriListSet('"+rename.key+"')",
				// 		{VAR_KEY: rename.key,
				// 			VAR_NAME:rename.name}, null, function(){
				// 			MessageBox.alert("Update successful");
				// 			},function(){
				// 		MessageBox.alert("Update failed");});
				// });
				sMessage = "Yeniden isimlendirilen varyant(lar): \n";
				if (renamed) {
					for (var h = 0; h < renamed.length; h++) {
						sMessage += "Varyantınız '" + renamed[h].name.toString() + "' olarak yeniden isimlendirildi!\n";
					}
				} else {
					sMessage += "Yok!";
				}
			}
			//delete backend data
			if (deleted) {
				for (var i = 0; i < deleted.length; i++) {
					for (var j = 0; j < globalVariantItemsList.length; j++) {
						if (globalVariantItemsList[j].VAR_KEY.toString() === deleted[i].toString()) {
							globalVariantItemsList.splice(j, 1);
						}
					}
				}
				var data = {
					"Prcss": "VarSil",
					"IslVariant": []
				};
				for (var x = 0; x < deleted.length; x++) {
					var Selectedrow = {
						"Variant": deleted[x].toString().substring(9, deleted[x].length),
						"Pname": "ONERILIST"
					};
					data.IslVariant.push(Selectedrow);
				}
				that.createModel = new JSONModel(data);
				that.getView().setModel(that.createModel, "createCollection");
				var oSOData = this.getView().getModel("createCollection").getData();
				var oModel = this.getOwnerComponent().getModel();
				oModel.create("/ProcessSet", oSOData, {
					method: "POST",
					header: "Content-Type: application/json",
					success: function (scc) {
						if (renamed) {
							sMessage += "\n\nSilinen Varyant(lar): \n";
						} else {
							sMessage += "Silinen Varyant(lar): \n";
						}
						if (deleted) {
							for (var f = 0; f < deleted.length; f++) {
								sMessage += "Varyant '" + deleted[f].substring(9, deleted[f].length) + "' silindi!\n";
							}
						} else {
							sMessage += "Yok";
						}
						oModel.refresh();
					},
					error: function (err) {
						sap.m.MessageToast.show("Silme başarısız!");
					}
				});
			}
			if (sMessage !== undefined) {
				sap.m.MessageToast.show(sMessage);
			} else {
				sap.m.MessageToast.show("İşlem başarısız!");
			}
		},

		onSelect: function (oEvent) {
			var that = this;
			var selectedKey = oEvent.getSource().getSelectionKey();
			if (selectedKey === "*standard*") {
				that.clearAllFields();
				var sMessage = "Standart olan 'Varyant' seçildi ve ekranın alanlarına uygulandı!";
				sap.m.MessageToast.show(sMessage);
			} else {
				var variantItemModel = new JSONModel();
				variantItemModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/VariantHeaderSet(Pname='ONERILIST',Variant='" + oEvent.getParameters().key
					.substring(9, oEvent.getParameters().key.length) + "')?$expand=NPVariant");
				variantItemModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						var j = 0,
							localSelectedNYTs = [];
						var k = 0,
							localAccountCodeKeys = [];
						var l = 0,
							localAccountCodeTitles = [];
						var variantItemData = oEvent.getSource().getData().d.NPVariant.results;
						if (variantItemData.length !== 0) {
							for (var i = 0; i < variantItemData.length; i++) {
								if (variantItemData[i].Field.toString() === "CompanyCode") {
									that.getView().byId("CompanyCode").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "globalSelectedCompanyCode") {
									globalSelectedCompanyCode = variantItemData[i].Value.toString();
								}
								if (variantItemData[i].Field.toString() === "OpenPaymentDate") {
									that.getView().byId("OpenPaymentDate").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "ReferenceDate") {
									that.getView().byId("ReferenceDate").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "Currency") {
									that.getView().byId("Currency").setValue(variantItemData[i].Value.toString());
									that.getView().byId("Currency").setSelectedKey(variantItemData[i].Value.toString().substr(0, variantItemData[i].Value.toString()
										.indexOf(' ')));
								}
								if (variantItemData[i].Field.toString() === "NYT") {
									that.getView().byId("NYT").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "globalSelectedNYTs") {
									if (variantItemData[i].Value.toString() !== undefined) {
										localSelectedNYTs[j] = variantItemData[i].Value.toString();
										j++;
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxNYT") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxNYT").setSelected(true);
										that.getView().byId("NYT").setEditable(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										// that.uncheckNYT();
										that.getView().byId("checkBoxNYT").setSelected(false);
										that.getView().byId("NYT").setEditable(false);
										globalSelectedNYTs = [];
										that.getView().byId("NYT").setEditable(false);
										that.getView().byId("NYT").setSelectedItems([]);
										that.getView().byId("NYT").setSelectedKeys([]);
									}
								}
								if (variantItemData[i].Field.toString() === "accountCode") {
									that.getView().byId("accountCode").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "globalAccountCodeKeys") {
									if (variantItemData[i].Value.toString() !== undefined) {
										localAccountCodeKeys[k] = variantItemData[i].Value.toString();
										k++;
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxAC") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxAC").setSelected(true);
										that.getView().byId("accountCode").setEditable(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										// that.uncheckAC();
										that.getView().byId("checkBoxAC").setSelected(false);
										that.getView().byId("accountCode").setEditable(false);
										globalAccountCodeKeys = [];
										globalAccountCodeTitles = [];
										that.byId("accountCode").removeAllTokens();
										// globalAccountCodeKey = "";
									}
								}
								if (variantItemData[i].Field.toString() === "PaymentCondition") {
									that.getView().byId("PaymentCondition").setValue(variantItemData[i].Value.toString());
									that.getView().byId("PaymentCondition").setSelectedKey(variantItemData[i].Value.toString().substr(0, variantItemData[i].Value
										.toString().indexOf(' ')));
								}
								if (variantItemData[i].Field.toString() === "PaymentKind") {
									that.getView().byId("PaymentKind").setValue(variantItemData[i].Value.toString());
									that.getView().byId("PaymentKind").setSelectedKey(variantItemData[i].Value.toString().substr(0, variantItemData[i].Value.toString()
										.indexOf(' ')));
								}
								if (variantItemData[i].Field.toString() === "ReferenceHeaderKey") {
									that.getView().byId("ReferenceHeaderKey").setValue(variantItemData[i].Value.toString());
								}
								if (variantItemData[i].Field.toString() === "checkBoxWithoutRHK") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxWithoutRHK").setSelected(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										that.getView().byId("checkBoxWithoutRHK").setSelected(false);
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxWithRHK") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxWithRHK").setSelected(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										that.getView().byId("checkBoxWithRHK").setSelected(false);
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxSperr") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxSperr").setSelected(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										that.getView().byId("checkBoxSperr").setSelected(false);
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxZahls") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxZahls").setSelected(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										that.getView().byId("checkBoxZahls").setSelected(false);
									}
								}
								if (variantItemData[i].Field.toString() === "checkBoxBorcBak") {
									if (variantItemData[i].Value.toString() === "true") {
										that.getView().byId("checkBoxBorcBak").setSelected(true);
									}
									if (variantItemData[i].Value.toString() === "false") {
										that.getView().byId("checkBoxBorcBak").setSelected(false);
									}
								}
								if (variantItemData[i].Field.toString() === "globalAccountCodeTitles") {
									if (variantItemData[i].Value.toString() !== undefined) {
										localAccountCodeTitles[l] = variantItemData[i].Value.toString();
										l++;
									}
								}
							}
							if (localSelectedNYTs.length !== 0) {
								that.getView().byId("NYT").setSelectedKeys(localSelectedNYTs);
								globalSelectedNYTs = that.getView().byId("NYT").getSelectedItems();
							}
							if (localAccountCodeKeys.length !== 0) {
								that.byId("accountCode").removeAllTokens();
								var oMultiInput = that.byId("accountCode");
								// that.getView().byId("accountCode").setSelectedKeys(localAccountCodeKeys);
								for (var i = 0; i < localAccountCodeKeys.length; i++) {
									oMultiInput.addToken(new Token({
										key: localAccountCodeKeys[i],
										// text: localAccountCodeTitles[i]
										text: localAccountCodeKeys[i]
									}));
								}
								that.byId("accountCode").setModel(oMultiInput);
								globalAccountCodeKeys = [];
								globalAccountCodeTitles = [];
								globalAccountCodeKeys = localAccountCodeKeys;
								globalAccountCodeTitles = localAccountCodeTitles;
							}
							var sMessage = "Varyant '" + selectedKey.substring(9, selectedKey.length) + "' seçildi ve ekranın alanlarına uygulandı!";
							sap.m.MessageToast.show(sMessage);
						} else {
							sap.m.MessageToast.show("Varyant seçimi başarısız!\nServisten veri dönmedi!");
						}
					}
				});
			}
		},

		getParametersValue: function () {
			var that = this;
			var parametersValue = Object.create(null);
			parametersValue.CompanyCode = that.getView().byId("CompanyCode").getValue();
			parametersValue.globalSelectedCompanyCode = globalSelectedCompanyCode;
			parametersValue.OpenPaymentDate = that.getView().byId("OpenPaymentDate").getValue();
			parametersValue.ReferenceDate = that.getView().byId("ReferenceDate").getValue();
			parametersValue.Currency = that.getView().byId("Currency").getValue();
			parametersValue.NYT = that.getView().byId("NYT").getValue();
			parametersValue.globalSelectedNYTs = [];
			parametersValue.globalSelectedNYTs = globalSelectedNYTs;
			parametersValue.checkBoxNYT = that.getView().byId("checkBoxNYT").getSelected();
			parametersValue.accountCode = that.getView().byId("accountCode").getValue();
			parametersValue.globalAccountCodeKeys = [];
			parametersValue.globalAccountCodeKeys = globalAccountCodeKeys;
			parametersValue.globalAccountCodeTitles = [];
			parametersValue.globalAccountCodeTitles = globalAccountCodeTitles;
			parametersValue.checkBoxAC = that.getView().byId("checkBoxAC").getSelected();
			parametersValue.PaymentCondition = that.getView().byId("PaymentCondition").getValue();
			parametersValue.PaymentKind = that.getView().byId("PaymentKind").getValue();
			parametersValue.ReferenceHeaderKey = that.getView().byId("ReferenceHeaderKey").getValue();
			parametersValue.checkBoxWithoutRHK = that.getView().byId("checkBoxWithoutRHK").getSelected();
			parametersValue.checkBoxWithRHK = that.getView().byId("checkBoxWithRHK").getSelected();
			parametersValue.checkBoxSperr = that.getView().byId("checkBoxSperr").getSelected();
			parametersValue.checkBoxZahls = that.getView().byId("checkBoxZahls").getSelected();
			parametersValue.checkBoxBorcBak = that.getView().byId("checkBoxBorcBak").getSelected();
			return parametersValue;
		},

		calculateTotalAmountOfSubPayments: function (popupItemList) {
			var totalVadeDoccur = 0;
			var totalVadeDoccurCurrency = "";
			var totalList = {
				"cost": "0,00",
				"pb": " "
			};
			totalVadeDoccurCurrency = popupItemList[0].Waers;
			for (var i = 0; i < popupItemList.length; i++) {
				totalVadeDoccur = totalVadeDoccur + parseFloat(popupItemList[i].Wrbtr, 10);
			}
			totalList.cost = totalVadeDoccur.toString();
			totalList.pb = totalVadeDoccurCurrency;
			var costModel = new JSONModel(totalList);
			costModel.setProperty("/dataSubList", totalList);
			sap.ui.getCore().setModel(costModel, "costSubData");
		},

		setDecimalPoints: function (strDigit) {
			var oNumberFormat = sap.ui.core.format.NumberFormat.getFloatInstance({
				maxFractionDigits: 2,
				groupingEnabled: true,
				groupingSeparator: ".",
				decimalSeparator: ","
			});
			if (strDigit !== undefined) {
				var n = -1;
				var str = strDigit;
				n = str.search(",");
				if (n !== -1) {
					str = str.replace(",", ".");
				} else {
					for (var i = 0; i < str.length; i++) {
						if (str[i] === ".") {
							n = i;
						}
					}
					if (n === -1) {
						str = str + ".00";
					}
				}
				var convertedStr = oNumberFormat.format(str);
				convertedStr = convertedStr.split('.').join("");
				convertedStr = convertedStr.replace(",", ".");
			}
			return convertedStr;
		},

		removeSubPayment: function (oEvent) {
			var that = this;
			var data = {
				"Prcss": "DetaySil",
				"Pernr": sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr(),
				"IslOneriDetay": []
			};
			var selectedRow = {
				"Bukrs": oEvent.getSource().getParent().getCells()[1].getProperty("text").toString(),
				"Gjahr": oEvent.getSource().getParent().getCells()[2].getProperty("text").toString(),
				"Buzei": oEvent.getSource().getParent().getCells()[3].getProperty("text").toString(),
				"Belnr": oEvent.getSource().getParent().getCells()[5].getProperty("text").toString()
			};
			var localMinusAmount = oEvent.getSource().getParent().getCells()[8].getProperty("number").toString();
			localMinusAmount = localMinusAmount.replace(".", "");
			localMinusAmount = localMinusAmount.replace(",", ".");
			data.IslOneriDetay.push(selectedRow);
			that.createModel = new JSONModel(data);
			that.getView().setModel(that.createModel, "createCollection");
			var oSOData = this.getView().getModel("createCollection").getData();
			var oModel = this.getOwnerComponent().getModel();
			oModel.create("/ProcessSet", oSOData, {
				method: "POST",
				header: "Content-Type: application/json",
				success: function (scc) {
					sap.m.MessageToast.show("Alt kalem silindi!");
					oModel.refresh();
					var totalSubList = {
						"cost": sap.ui.getCore().getModel("costSubData").oData.cost.toString(),
						"pb": sap.ui.getCore().getModel("costSubData").oData.pb.toString()
					};
					totalSubList.cost = totalSubList.cost - parseFloat(localMinusAmount, 10);
					var costModel = new JSONModel(totalSubList);
					costModel.setProperty("/dataSubList", totalSubList);
					sap.ui.getCore().setModel(costModel, "costSubData");

					var strTotalSubListCost = that.setDecimalPoints(totalSubList.cost.toString());
					globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Odtut = strTotalSubListCost;
					var localAmountDifference = parseFloat(globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].VadeDoccur,
						10) - parseFloat(globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Odtut, 10);
					localAmountDifference = that.setDecimalPoints(localAmountDifference.toString());
					globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Fark = localAmountDifference.toString();
					var cModel = new JSONModel(globalOneriListesiDetayData);
					that.getView().setModel(cModel, "oneriListesiDetayList");

					var localLifnr = globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Lifnr;
					var localPB = globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Currency;
					var localGsber = globalOneriListesiDetayData[globalDetailSuggestedItemSource.getParent().getIndex()].Gsber;
					var oneriListesiDetayItemModel = new JSONModel();
					oneriListesiDetayItemModel.loadData("/sap/opu/odata/sap/ZEO_GW_SRV/OneriDetaySet?$filter=Lifnr eq '" + localLifnr +
						"'  and Waers eq '" + localPB +
						"' and Gsber eq '" + localGsber + "' and Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserPernr() +
						"'");
					oneriListesiDetayItemModel.attachRequestCompleted(function onComplete(oEvent2) {
						if (oEvent2.getParameter("success")) {
							var oneriListesiDetayItemData = oEvent2.getSource().getData().d.results;
							oneriListesiDetayItemModel.setProperty("/itemList", oneriListesiDetayItemData);
							that.calculateTotalAmountOfSubPayments(oneriListesiDetayItemData);
							globalQuickView.setModel(oneriListesiDetayItemModel, "item");
						}
					});

					that.calculateTotalAmount();

				},
				error: function (err) {
					sap.m.MessageToast.show("Alt kalem silme başarısız!");
				}
			});
		},
		
		changeNoDataText: function (oEvent) {
			this.getView().byId("oneriListesi").setNoData("Veri Bulunamadı!");
		}

	});

});