sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/core/Fragment",
	"sap/m/MessageToast",
	"sap/m/MessageBox",
	"sap/ui/model/Filter",
	"sap/ui/model/FilterOperator"
], function (Controller, JSONModel, Fragment, MessageToast, MessageBox, Filter, FilterOperator) {
	"use strict";

	return Controller.extend("MDP_E-Odeme.FIEodeme.controller.OdemeTaleplerimReport", {
		onInit: function () {
			var that = this;
			this.getView().byId("userNameSurname").setText("Hoş Geldiniz, " + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login").getGlobalUserNameSurname());
			this.getView().addEventDelegate({
				onBeforeShow: function (evt) {
					that.getParams(that);
				}
			});
			this.byId("DRS2").setDateValue(new Date());
			this.byId("DRS2").setSecondDateValue(new Date());
		},
		getParams: function (e) {
			var that = this;
			var table = this.getView().byId("odemeTaleplerim");
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			var routeParam = oRouter.getRoute("Target_Dashboard")._aRoutes[0]._router._prevRoutes[0].params[0];
			var OdemeTaleplerimModel = new JSONModel();
			if (routeParam === "bir") {
				OdemeTaleplerimModel.loadData(
					"/sap/opu/odata/sap/ZEO_GW_SRV/OdmTaleplerimSet?$filter=Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login")
					.getGlobalUserPernr() + "' and (Ostat eq '02' or Ostat eq '06' or Ostat eq '07')"
				);
			} else if (routeParam === "iki") {
				OdemeTaleplerimModel.loadData(
					"/sap/opu/odata/sap/ZEO_GW_SRV/OdmTaleplerimSet?$filter=Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login")
					.getGlobalUserPernr() + "' and (Ostat eq '09')"
				);
			} else if (routeParam === "uc") {
				OdemeTaleplerimModel.loadData(
					"/sap/opu/odata/sap/ZEO_GW_SRV/OdmTaleplerimSet?$filter=Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login")
					.getGlobalUserPernr() + "' and (Ostat eq '04' or Ostat eq '08' or Ostat eq '10')"
				);
			} else {
				OdemeTaleplerimModel.loadData(
					"/sap/opu/odata/sap/ZEO_GW_SRV/OdmTaleplerimSet?$filter=Pernr eq '" + sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Login")
					.getGlobalUserPernr() + "'"
				);
				// var vizFrame = this.getView().byId("chart2");
				// vizFrame.removeAllFeeds();
				var oModel = new JSONModel();
				// var dashboardGraphicModel = new JSONModel();
				OdemeTaleplerimModel.attachRequestCompleted(function onComplete(oEvent) {
					if (oEvent.getParameter("success")) {
						sap.ui.core.BusyIndicator.hide();
						var data = oEvent.getSource().getData().d.results;

						var reduceDataOdmta =
							data.reduce(function (r, a) {
								r[a.OstatDesc] = r[a.OstatDesc] || [];
								r[a.OstatDesc].push(a);
								return r;
							}, {});
						var OstatDescArr = Object.entries(reduceDataOdmta);

						var endData = [];
						for (var j = 0; j < OstatDescArr.length; j++) {
							endData.push({
								"name": OstatDescArr[j][0],
								"number": OstatDescArr[j][1].length
							});
						}

						var newModel = new JSONModel();
						newModel.setProperty("/dataList", endData);
						that.getView().setModel(newModel, "OdemeTaleplerim");
						oModel.setData(endData);
					}
				});
				// var dataset = new sap.viz.ui5.data.FlattenedDataset({
				// 	dimensions: [{
				// 		name: "Ödeme Türü",
				// 		value: "{name}"
				// 	}],
				// 	measures: [{
				// 		name: "Toplam",
				// 		value: "{number}"
				// 	}],
				// 	data: {
				// 		path: "OdemeTaleplerim>/dataList"
				// 	}
				// });
				// vizFrame.setDataset(dataset);
				// vizFrame.setModel(oModel);

				// vizFrame.setVizProperties({
				// 	title: {
				// 		text: "Ödeme Türüne Göre Detaylar"
				// 	},
				// 	plotArea: {
				// 		colorPalette: d3.scale.category20().range(),
				// 		drawwingEffect: "glossy"
				// 	}
				// });
				// var feedsize = new sap.viz.ui5.controls.common.feeds.FeedItem({
				// 		uid: "size",
				// 		type: "Measure",
				// 		values: ["Toplam"]
				// 	}),
				// 	feedcolor = new sap.viz.ui5.controls.common.feeds.FeedItem({
				// 		uid: "color",
				// 		type: "Dimension",
				// 		values: ["Ödeme Türü"]
				// 	});
				// vizFrame.addFeed(feedsize);
				// vizFrame.addFeed(feedcolor);
			}
			OdemeTaleplerimModel.attachRequestCompleted(function onComplete(oEvent) {
				if (oEvent.getParameter("success")) {
					sap.ui.core.BusyIndicator.hide();
					var data = oEvent.getSource().getData().d.results;
					for (var i = 0; i < data.length; i++) {
						data[i].Cpudt = sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.Dashboard").unixToDate1(data[i].Cpudt);
					}
					table.setVisibleRowCountMode("Fixed");
					var x = window.innerHeight - 213;
					var count = Math.floor(x / 45);
					table.setVisibleRowCount(count - 3);
					// table.setVisibleRowCountMode("Fixed");
					// table.setVisibleRowCount(data.length + 1);
					// table.setVisibleRowCount(20);
					var newModel = new JSONModel();
					newModel.setProperty("/dataList", data);
					that.getView().setModel(newModel, "OdemeTaleplerim");

					var costs = [];
					for (var j = 0; j < data.length; j++) {
						costs.push({
							pb: data[j].Waers,
							cost: parseFloat(data[j].Wrbtr)
						});
					}
					var result = [];
					costs.reduce(function (res, value) {
						if (!res[value.pb]) {
							res[value.pb] = {
								pb: value.pb,
								cost: 0
							};
							result.push(res[value.pb]);
						}
						res[value.pb].cost += value.cost;
						return res;
					}, {});

					var costModel = new JSONModel(result);
					costModel.setProperty("/dataList", result);
					that.getView().setModel(costModel, "costData");
				}
			});
		},

		goHome: function (oEvent) {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("Target_Dashboard");
		},

		goOut: function (e) {
			var that = this;
			MessageBox.show(
				"Çıkış yapmak istediğinize emin misiniz?", {
					icon: MessageBox.Icon.INFORMATION,
					title: "Uyarı",
					actions: [MessageBox.Action.YES, MessageBox.Action.NO],
					emphasizedAction: MessageBox.Action.YES,
					onClose: function (sButton) {
						if (sButton === MessageBox.Action.YES) {
							var globalOStorage = jQuery.sap.storage(jQuery.sap.storage.Type.local);
							globalOStorage.clear();
							var oRouter = sap.ui.core.UIComponent.getRouterFor(that);
							sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OdemeTalebiGirisi").clearAllFields(oRouter._oRoutes.Target_Odeme_Talebi_Girisi._oRouter._oViews._oCache.view["MDP_E-Odeme.FIEodeme.view.OdemeTalebiGirisi"].undefined);
							oRouter.navTo("Login");
							window.history.pushState(null, "", window.location.href);
							window.history.pushState(null, "", window.location.href);
							// window.onpopstate = function () {
							// 	window.history.pushState(null, "", window.location.href);
							// };
						}
					}
				}
			);
		},

		detailApprover: function (e) {
			var table = this.getView().byId("odemeTaleplerim");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").DetailApprover(e, Odmfn);
		},
		deleteData: function (e) {
			var that = this;
			var table = this.getView().byId("odemeTaleplerim");
			var Odmfn = table.getContextByIndex(e.getSource().getParent().getIndex()).getObject().Odmfn;
			// var index = table.getSelectedIndices();

			var data = {
				"Prcss": "Sil",
				"IslOdmTaleplerim": []
			};

			var Selectedrow = {
				"Odmfn": Odmfn
			};
			data.IslOdmTaleplerim.push(Selectedrow);

			that.createModel = new JSONModel(data);
			that.getView().setModel(that.createModel, "createCollection");
			var oSOData = this.getView().getModel("createCollection").getData();

			var oModel = this.getOwnerComponent().getModel();

			oModel.create("/ProcessSet", oSOData, {
				method: "POST",
				header: "Content-Type: application/json",
				success: function (scc) {
					MessageToast.show(Odmfn + " " + "Numaralı Form Başarı ile Silinmiştir.");
					that.getParams(e);
				},
				error: function (err) {
					MessageToast.show(JSON.parse(err.responseText).error.innererror.errordetails[0].message);
				}
			});
		},
		showDetailForm: function (e) {
			var table = this.getView().byId("odemeTaleplerim");
			var view = this.getView();
			sap.ui.controller("MDP_E-Odeme.FIEodeme.controller.OnayimdaBekleyenTalepler").ShowDetailOdemeFormn(e, table, view);
		},
		handleChange: function (e) {

			var dateFrom = e.getSource().getDateValue();
			var dateTo = e.getSource().getSecondDateValue();
			dateFrom.setHours(3, 0, 0, 0);
			dateTo.setHours(23, 59, 59, 59);
			// dateFrom = dateFrom.toISOString().split(".")[0];
			// dateTo = dateTo.toISOString().split(".")[0];

			var filter = [];
			var list = this.getView().byId("odemeTaleplerim");

			var oBinding = list.getBinding();

			if (dateFrom !== null && dateTo !== null) {
				filter.push(new sap.ui.model.Filter("Cpudt", FilterOperator.BT, dateFrom, dateTo));
			}
			oBinding.filter(filter);

		}
	});
});