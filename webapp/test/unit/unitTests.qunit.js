/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"MDP_E-Odeme/FIEodeme/test/unit/AllTests"
	], function () {
		QUnit.start();
	});
});