/*global QUnit*/

sap.ui.define([
	"MDP_E-Odeme/FIEodeme/controller/OdemeTalebiGirisi.controller"
], function (Controller) {
	"use strict";

	QUnit.module("OdemeTalebiGirisi Controller");

	QUnit.test("I should test the OdemeTalebiGirisi controller", function (assert) {
		var oAppController = new Controller();
		oAppController.onInit();
		assert.ok(oAppController);
	});

});