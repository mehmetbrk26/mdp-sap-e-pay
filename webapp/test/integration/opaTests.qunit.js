/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"MDP_E-Odeme/FIEodeme/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});